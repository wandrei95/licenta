package com.wegroszta.andrei.licenta.io.auth;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.wegroszta.andrei.licenta.entities.Credentials;
import com.wegroszta.andrei.licenta.entities.User;
import com.wegroszta.andrei.licenta.usecases.auth.UserCreator;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;

public class FirebaseUserCreator implements UserCreator {
    private final FirebaseAuth firebaseAuth;
    private final DatabaseReference baseDatabaseReference;

    public FirebaseUserCreator(final FirebaseAuth firebaseAuth, final DatabaseReference baseDatabaseReference) {
        this.firebaseAuth = firebaseAuth;
        this.baseDatabaseReference = baseDatabaseReference;
    }

    @Override
    public Observable<Void> signUp(final Credentials credentials, final User user) {
        return Observable.create(e -> firebaseAuth.createUserWithEmailAndPassword(credentials.getEmail(), credentials.getPassword())
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        saveCreatedUserData(user, e);
                    } else {
                        e.onError(new RuntimeException("Login Failed"));
                    }
                }));
    }

    private void saveCreatedUserData(final User user, final ObservableEmitter<Void> e) {
        DatabaseReference databaseReference = getPatientDetailsDbReference();
        databaseReference.setValue(user)
                .addOnCompleteListener(t -> e.onComplete())
                .addOnFailureListener(e::onError);
    }

    private DatabaseReference getPatientDetailsDbReference() {
        String userId = firebaseAuth.getCurrentUser().getUid();
        return baseDatabaseReference.child("patients")
                .child(userId)
                .child("details");
    }
}