package com.wegroszta.andrei.licenta.io.measurement;

import com.google.firebase.database.DatabaseReference;
import com.wegroszta.andrei.licenta.entities.HeartRate;
import com.wegroszta.andrei.licenta.usecases.measurement.heartrate.HeartRateSaver;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;

public class FirebaseHeartRateSaver implements HeartRateSaver {
    private final DatabaseReference baseDatabaseReference;

    public FirebaseHeartRateSaver(final DatabaseReference baseDatabaseReference) {
        this.baseDatabaseReference = baseDatabaseReference;
    }

    @Override
    public Observable<Void> saveHeartRate(final String userId, final HeartRate heartRate) {
        return Observable.create(source -> saveHeartRate(userId, heartRate, source));
    }

    private void saveHeartRate(final String userId, final HeartRate heartRate,
                               final ObservableEmitter<Void> source) {
        getHeartRateDatabaseReferenceForUser(userId)
                .setValue(heartRate)
                .addOnCompleteListener(task -> source.onComplete())
                .addOnFailureListener(source::onError);
    }

    private DatabaseReference getHeartRateDatabaseReferenceForUser(final String userId) {
        return baseDatabaseReference.child("measurement")
                .child("heartRate")
                .child(userId)
                .child("values").push();
    }
}
