package com.wegroszta.andrei.licenta.viewmodels.statistics;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.wegroszta.andrei.licenta.entities.HeartRate;
import com.wegroszta.andrei.licenta.usecases.auth.UserDataFetcher;
import com.wegroszta.andrei.licenta.usecases.statistics.heartrate.FetchHeartRateStatistics;
import com.wegroszta.andrei.licenta.viewmodels.responses.Response;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class FetchHeartRateStatisticsViewModel extends ViewModel {
    private final FetchHeartRateStatistics fetchHeartRateStatistics;
    private final UserDataFetcher userDataFetcher;
    private final MutableLiveData<Response<List<HeartRate>>> fetchHeartRateResponse;
    private final CompositeDisposable disposables;

    public FetchHeartRateStatisticsViewModel(final FetchHeartRateStatistics fetchHeartRateStatistics,
                                             final UserDataFetcher userDataFetcher) {
        this.fetchHeartRateStatistics = fetchHeartRateStatistics;
        this.userDataFetcher = userDataFetcher;
        fetchHeartRateResponse = new MutableLiveData<>();
        disposables = new CompositeDisposable();
    }

    @Override
    protected void onCleared() {
        disposables.clear();
    }

    public MutableLiveData<Response<List<HeartRate>>> getFetchHeartRateResponse() {
        return fetchHeartRateResponse;
    }

    public void getHeartRates() {
        final String userId = userDataFetcher.getLoggedUserId();
        disposables.add(fetchHeartRateStatistics.observeHeartRateStatistics(userId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> fetchHeartRateResponse.setValue(Response.started()))
                .subscribe(
                        heartRates -> fetchHeartRateResponse.setValue(Response.success(heartRates)),
                        throwable -> fetchHeartRateResponse.setValue(Response.failure(throwable)),
                        () -> fetchHeartRateResponse.setValue(Response.completed(null))
                )
        );
    }
}
