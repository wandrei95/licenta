package com.wegroszta.andrei.licenta.ui.doctors;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wegroszta.andrei.licenta.R;
import com.wegroszta.andrei.licenta.entities.Doctor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DoctorsRecyclerViewAdapter extends RecyclerView.Adapter<DoctorsRecyclerViewAdapter.DoctorsViewHolder> {
    private final List<Doctor> doctors;
    private final List<Doctor> filteredDoctors;
    private OnDoctorClickListener onDoctorClickListener;

    DoctorsRecyclerViewAdapter() {
        this.doctors = new ArrayList<>();
        this.filteredDoctors = new ArrayList<>();
    }

    public void setDoctors(final Collection<Doctor> doctors) {
        this.doctors.clear();
        this.doctors.addAll(doctors);
        this.filteredDoctors.clear();
        this.filteredDoctors.addAll(doctors);
        notifyDataSetChanged();
    }

    public void setOnDoctorClickListener(final OnDoctorClickListener onDoctorClickListener) {
        this.onDoctorClickListener = onDoctorClickListener;
    }

    @NonNull
    @Override
    public DoctorsViewHolder onCreateViewHolder(@NonNull final ViewGroup parent,
                                                final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.doctor_layout, parent, false);
        return new DoctorsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final DoctorsViewHolder holder,
                                 final int position) {
        Doctor doctor = filteredDoctors.get(position);
        String name = doctor.getFirstName() + " " + doctor.getLastName();

        holder.tvName.setText(name);
        holder.container.setOnClickListener(v -> {
            if (onDoctorClickListener != null) {
                onDoctorClickListener.onDoctorClicked(doctor);
            }
        });
    }

    @Override
    public int getItemCount() {
        return filteredDoctors.size();
    }

    public void filter(final String filterText) {
        filteredDoctors.clear();

        if (filterText == null || filterText.trim().isEmpty()) {
            filteredDoctors.addAll(doctors);
            notifyDataSetChanged();
        } else {
            for (Doctor doctor : doctors) {
                if (doctor.getFirstName().toUpperCase().contains(filterText.toUpperCase())
                        || doctor.getLastName().toUpperCase().contains(filterText.toUpperCase())) {
                    filteredDoctors.add(doctor);
                }
            }
            notifyDataSetChanged();
        }
    }

    class DoctorsViewHolder extends RecyclerView.ViewHolder {
        final TextView tvName;
        final View container;

        DoctorsViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            container = itemView;
        }
    }

    public interface OnDoctorClickListener {
        void onDoctorClicked(Doctor doctor);
    }
}
