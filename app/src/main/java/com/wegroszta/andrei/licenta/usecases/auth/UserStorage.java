package com.wegroszta.andrei.licenta.usecases.auth;

public interface UserStorage {
    String getLoggedUserId();
}
