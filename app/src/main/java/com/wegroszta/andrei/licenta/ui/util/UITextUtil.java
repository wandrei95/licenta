package com.wegroszta.andrei.licenta.ui.util;

import android.widget.EditText;

public class UITextUtil {
    private UITextUtil() {

    }

    public static void setTextToEditText(final EditText et, final String text) {
        if (text != null) {
            String trimmed = text.trim();
            if (!trimmed.isEmpty()) {
                et.setText(trimmed);
            }
        }
    }
}