package com.wegroszta.andrei.licenta.viewmodels.doctors;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.wegroszta.andrei.licenta.entities.Doctor;
import com.wegroszta.andrei.licenta.usecases.auth.UserDataFetcher;
import com.wegroszta.andrei.licenta.usecases.doctors.GetDoctors;
import com.wegroszta.andrei.licenta.viewmodels.responses.Response;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class DoctorsViewModel extends ViewModel {
    private final GetDoctors getDoctors;
    private final UserDataFetcher userDataFetcher;
    private final MutableLiveData<Response<List<Doctor>>> getDoctorsResponse;
    private final MutableLiveData<Response<Doctor>> getDoctorResponse;
    private final CompositeDisposable disposables;

    public DoctorsViewModel(final GetDoctors getDoctors, final UserDataFetcher userDataFetcher) {
        this.getDoctors = getDoctors;
        this.userDataFetcher = userDataFetcher;
        getDoctorsResponse = new MutableLiveData<>();
        getDoctorResponse = new MutableLiveData<>();
        disposables = new CompositeDisposable();
    }

    @Override
    protected void onCleared() {
        disposables.clear();
    }

    public MutableLiveData<Response<List<Doctor>>> getGetDoctorsResponse() {
        return getDoctorsResponse;
    }

    public MutableLiveData<Response<Doctor>> getGetDoctorResponse() {
        return getDoctorResponse;
    }

    public void getAllDoctors() {
        disposables.add(getDoctors.getAllDoctors()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getDoctorsResponse.setValue(Response.started()))
                .subscribe(
                        doctors -> getDoctorsResponse.setValue(Response.success(doctors)),
                        throwable -> getDoctorsResponse.setValue(Response.failure(throwable)),
                        () -> getDoctorsResponse.setValue(Response.completed(null))
                )
        );
    }

    public void getDoctorsForCurrentPatient() {
        final String userId = userDataFetcher.getLoggedUserId();
        disposables.add(getDoctors.getDoctorsForPatient(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getDoctorsResponse.setValue(Response.started()))
                .subscribe(
                        doctors -> getDoctorsResponse.setValue(Response.success(doctors)),
                        throwable -> getDoctorsResponse.setValue(Response.failure(throwable)),
                        () -> getDoctorsResponse.setValue(Response.completed(null))
                )
        );
    }

    public void getDoctor(final String doctorId) {
        disposables.add(getDoctors.getDoctor(doctorId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getDoctorResponse.setValue(Response.started()))
                .subscribe(
                        doctor -> getDoctorResponse.setValue(Response.success(doctor)),
                        throwable -> getDoctorResponse.setValue(Response.failure(throwable)),
                        () -> getDoctorResponse.setValue(Response.completed(null))
                )
        );
    }
}
