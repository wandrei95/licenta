package com.wegroszta.andrei.licenta.entities;

public class StateData extends Data {
    private State state;

    public StateData() {
        this(0L, State.AVG);
    }

    public StateData(final long timestamp, final State state) {
        super(timestamp);
        this.state = state;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}
