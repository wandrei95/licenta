package com.wegroszta.andrei.licenta.ui.common;

import com.jjoe64.graphview.series.DataPoint;
import com.wegroszta.andrei.licenta.entities.StateValueData;
import com.wegroszta.andrei.licenta.entities.Temperature;

public class StateValueDataPoint extends DataPoint {
    private final StateValueData data;

    public StateValueDataPoint(final double x, final double y, StateValueData data) {
        super(x, y);
        this.data = data;
    }

    public StateValueData getData() {
        return data;
    }
}