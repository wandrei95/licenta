package com.wegroszta.andrei.licenta.entities;

public class Doctor extends User {
    private String id;
    private final String phone;
    private final String address;
    private final String specialty;

    public Doctor() {
        super();
        id = null;
        phone = null;
        address = null;
        specialty = null;
    }

    public Doctor(final String firstName, final String lastName, final String email,
                  final String phone, final String address, final String specialty) {
        super(firstName, lastName, email);
        id = null;
        this.phone = phone;
        this.address = address;
        this.specialty = specialty;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public String getSpecialty() {
        return specialty;
    }
}
