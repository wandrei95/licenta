package com.wegroszta.andrei.licenta.io.statistics;


import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.wegroszta.andrei.licenta.entities.HeartRate;
import com.wegroszta.andrei.licenta.usecases.statistics.heartrate.HeartRateStatisticsFetcher;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;

public class FirebaseHeartRateStatisticsFetcher implements HeartRateStatisticsFetcher {
    private final DatabaseReference baseDatabaseReference;

    public FirebaseHeartRateStatisticsFetcher(final DatabaseReference baseDatabaseReference) {
        this.baseDatabaseReference = baseDatabaseReference;
    }

    @Override
    public Observable<List<HeartRate>> getHeartRate(final String userId) {
        return Observable.create(source -> fetchHeartRates(userId, source));
    }

    private void fetchHeartRates(final String userId, final ObservableEmitter<List<HeartRate>> source) {
        DatabaseReference databaseReference = getDatabaseReference(userId);
        databaseReference.addListenerForSingleValueEvent(createValueEventListener(source));
    }

    @NonNull
    private ValueEventListener createValueEventListener(
            final ObservableEmitter<List<HeartRate>> source) {
        return new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                readHeartRate(dataSnapshot, source);
            }

            @Override
            public void onCancelled(final DatabaseError databaseError) {
                source.onError(databaseError.toException());
            }
        };
    }

    private void readHeartRate(final DataSnapshot dataSnapshot,
                               final ObservableEmitter<List<HeartRate>> source) {
        List<HeartRate> heartRates = getHeartRatesFromDataSnapshot(dataSnapshot);
        source.onNext(heartRates);
        source.onComplete();
    }

    @NonNull
    private List<HeartRate> getHeartRatesFromDataSnapshot(final DataSnapshot dataSnapshot) {
        List<HeartRate> heartRates = new ArrayList<>();
        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
            HeartRate heartRate = postSnapshot.getValue(HeartRate.class);
            heartRates.add(heartRate);
        }
        return heartRates;
    }

    private DatabaseReference getDatabaseReference(final String userId) {
        return baseDatabaseReference
                .child("measurement")
                .child("heartRate")
                .child(userId)
                .child("values");
    }
}
