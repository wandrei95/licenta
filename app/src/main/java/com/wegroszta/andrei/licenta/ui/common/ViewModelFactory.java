package com.wegroszta.andrei.licenta.ui.common;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.wegroszta.andrei.licenta.io.auth.FirebaseAuthenticator;
import com.wegroszta.andrei.licenta.io.auth.FirebaseUserCreator;
import com.wegroszta.andrei.licenta.io.auth.FirebaseUserStorage;
import com.wegroszta.andrei.licenta.io.doctors.FirebaseDoctorsStorage;
import com.wegroszta.andrei.licenta.io.measurement.BluetoothHeartRateMeasurer;
import com.wegroszta.andrei.licenta.io.measurement.BluetoothTemperatureMeasurer;
import com.wegroszta.andrei.licenta.io.measurement.FirebaseHeartRateSaver;
import com.wegroszta.andrei.licenta.io.measurement.FirebaseTemperatureSaver;
import com.wegroszta.andrei.licenta.io.patients.FirebasePatientsStorage;
import com.wegroszta.andrei.licenta.io.doctors.FirebaseDoctorCollaborationRequester;
import com.wegroszta.andrei.licenta.io.statistics.FirebaseHeartRateStatisticsFetcher;
import com.wegroszta.andrei.licenta.io.statistics.FirebaseTemperatureStatisticsFetcher;
import com.wegroszta.andrei.licenta.usecases.auth.Login;
import com.wegroszta.andrei.licenta.usecases.auth.UserDataFetcher;
import com.wegroszta.andrei.licenta.usecases.doctors.GetDoctors;
import com.wegroszta.andrei.licenta.usecases.measurement.heartrate.MeasureHeartRate;
import com.wegroszta.andrei.licenta.usecases.measurement.temperature.MeasureTemperature;
import com.wegroszta.andrei.licenta.usecases.patients.PatientsInteractor;
import com.wegroszta.andrei.licenta.usecases.doctors.RequestDoctorCollaboration;
import com.wegroszta.andrei.licenta.usecases.statistics.heartrate.FetchHeartRateStatistics;
import com.wegroszta.andrei.licenta.usecases.statistics.temperature.FetchTemperatureStatistics;
import com.wegroszta.andrei.licenta.viewmodels.myprofile.PatientsViewModel;
import com.wegroszta.andrei.licenta.viewmodels.auth.LoginViewModel;
import com.wegroszta.andrei.licenta.viewmodels.doctors.DoctorsViewModel;
import com.wegroszta.andrei.licenta.viewmodels.measurement.MeasureHeartRateViewModel;
import com.wegroszta.andrei.licenta.viewmodels.measurement.MeasureTemperatureViewModel;
import com.wegroszta.andrei.licenta.viewmodels.doctors.DoctorCollaborationViewModel;
import com.wegroszta.andrei.licenta.viewmodels.statistics.FetchHeartRateStatisticsViewModel;
import com.wegroszta.andrei.licenta.viewmodels.statistics.FetchTemperatureStatisticsViewModel;

public class ViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private static final Object lock = new Object();
    private static ViewModelFactory instance;

    private ViewModelFactory() {

    }

    public static ViewModelFactory getInstance() {
        if (instance == null) {
            synchronized (lock) {
                if (instance == null) {
                    instance = new ViewModelFactory();
                }
            }
        }
        return instance;
    }

    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull final Class<T> modelClass) {
        if (modelClass == LoginViewModel.class) {
            return (T) createNewLoginViewModel();
        } else if (modelClass == FetchHeartRateStatisticsViewModel.class) {
            return (T) createNewFetchHeartRateStatisticsViewModel();
        } else if (modelClass == FetchTemperatureStatisticsViewModel.class) {
            return (T) createNewFetchTemperatureStatisticsViewModel();
        } else if (modelClass == DoctorCollaborationViewModel.class) {
            return (T) createNewRequestDoctorCollaborationPresenter();
        } else if (modelClass == MeasureHeartRateViewModel.class) {
            return (T) createNewMeasureHeartRateViewModel();
        } else if (modelClass == MeasureTemperatureViewModel.class) {
            return (T) createNewMeasureTemperatureViewModel();
        } else if (modelClass == DoctorsViewModel.class) {
            return (T) createNewGetDoctorsViewModel();
        } else if (modelClass == PatientsViewModel.class) {
            return (T) createNewGetPatientsViewModel();
        }
        return super.create(modelClass);
    }

    @NonNull
    private LoginViewModel createNewLoginViewModel() {
        return new LoginViewModel(new Login(new FirebaseAuthenticator(FirebaseAuth.getInstance()),
                new FirebaseUserCreator(FirebaseAuth.getInstance(), FirebaseDatabase.getInstance().getReference())));
    }

    @NonNull
    private FetchHeartRateStatisticsViewModel createNewFetchHeartRateStatisticsViewModel() {
        return new FetchHeartRateStatisticsViewModel(
                new FetchHeartRateStatistics(new FirebaseHeartRateStatisticsFetcher(FirebaseDatabase.getInstance().getReference())),
                new UserDataFetcher(new FirebaseUserStorage(FirebaseAuth.getInstance())));
    }

    private FetchTemperatureStatisticsViewModel createNewFetchTemperatureStatisticsViewModel() {
        return new FetchTemperatureStatisticsViewModel(
                new FetchTemperatureStatistics(new FirebaseTemperatureStatisticsFetcher(FirebaseDatabase.getInstance().getReference())),
                new UserDataFetcher(new FirebaseUserStorage(FirebaseAuth.getInstance())));
    }

    @NonNull
    private DoctorCollaborationViewModel createNewRequestDoctorCollaborationPresenter() {
        return new DoctorCollaborationViewModel(
                new RequestDoctorCollaboration(new FirebaseDoctorCollaborationRequester(FirebaseDatabase.getInstance().getReference())),
                new UserDataFetcher(new FirebaseUserStorage(FirebaseAuth.getInstance())),
                new PatientsInteractor(new FirebasePatientsStorage(FirebaseDatabase.getInstance().getReference())));
    }

    @NonNull
    private MeasureHeartRateViewModel createNewMeasureHeartRateViewModel() {
        return new MeasureHeartRateViewModel(new MeasureHeartRate(
                new FirebaseHeartRateSaver(FirebaseDatabase.getInstance().getReference()), new BluetoothHeartRateMeasurer()),
                new UserDataFetcher(new FirebaseUserStorage(FirebaseAuth.getInstance())));
    }

    @NonNull
    private MeasureTemperatureViewModel createNewMeasureTemperatureViewModel() {
        return new MeasureTemperatureViewModel(new MeasureTemperature(
                new FirebaseTemperatureSaver(FirebaseDatabase.getInstance().getReference()), new BluetoothTemperatureMeasurer()),
                new UserDataFetcher(new FirebaseUserStorage(FirebaseAuth.getInstance())));
    }

    @NonNull
    private DoctorsViewModel createNewGetDoctorsViewModel() {
        return new DoctorsViewModel(new GetDoctors(new FirebaseDoctorsStorage(FirebaseDatabase.getInstance().getReference())),
                new UserDataFetcher(new FirebaseUserStorage(FirebaseAuth.getInstance())));
    }

    @NonNull
    private PatientsViewModel createNewGetPatientsViewModel() {
        return new PatientsViewModel(
                new PatientsInteractor(new FirebasePatientsStorage(FirebaseDatabase.getInstance().getReference())),
                new UserDataFetcher(new FirebaseUserStorage(FirebaseAuth.getInstance())));
    }
}
