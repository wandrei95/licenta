package com.wegroszta.andrei.licenta.usecases.measurement.temperature;

import com.wegroszta.andrei.licenta.entities.State;
import com.wegroszta.andrei.licenta.entities.Temperature;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;

public class MeasureTemperature {
    private static final int TEMPERATURE_MIN_AVG_VALUE = 34;
    private static final int TEMPERATURE_MAX_AVG_VALUE = 38;
    private final TemperatureSaver temperatureSaver;
    private final TemperatureMeasurer temperatureMeasurer;

    private double measuredTemperatureAvg;

    public MeasureTemperature(final TemperatureSaver temperatureSaver,
                              final TemperatureMeasurer temperatureMeasurer) {
        this.temperatureSaver = temperatureSaver;
        this.temperatureMeasurer = temperatureMeasurer;
    }

    public Observable<Temperature> observeTemperature() {
        measuredTemperatureAvg = 0d;
        return Observable.create(source ->
                temperatureMeasurer.observeTemperature()
                        .subscribe(temperature -> handleMeasuredTemperature(source, temperature),
                                throwable -> handleMeasuredTemperatureError(source, throwable)));
    }

    private void handleMeasuredTemperatureError(ObservableEmitter<Temperature> source, Throwable throwable) {
        if (!source.isDisposed()) {
            source.onError(throwable);
        }
    }

    public void stopMeasuring() {
        temperatureMeasurer.stopMeasuring();
    }

    private void handleMeasuredTemperature(final ObservableEmitter<Temperature> source,
                                           final Temperature temperature) {
        computeMeasuredTemperatureAvg(temperature);
        State state = computeTemperatureState(temperature.getValue());
        source.onNext(new Temperature(temperature.getTimestamp(), state, measuredTemperatureAvg));
    }

    private void computeMeasuredTemperatureAvg(final Temperature temperature) {
        if (measuredTemperatureAvg == 0d) {
            measuredTemperatureAvg = temperature.getValue();
        } else {
            measuredTemperatureAvg = (measuredTemperatureAvg + temperature.getValue()) / 2;
        }
    }

    private State computeTemperatureState(final double temperatureValue) {
        if (temperatureValue <= TEMPERATURE_MIN_AVG_VALUE) {
            return State.BELOW_AVG;
        } else if (temperatureValue <= TEMPERATURE_MAX_AVG_VALUE) {
            return State.AVG;
        } else {
            return State.ABOVE_AVG;
        }
    }

    public Observable<Void> saveTemperature(final String userId, final double value) {
        Temperature temperature = new Temperature(System.currentTimeMillis(), computeTemperatureState(value), value);
        return temperatureSaver.saveTemperature(userId, temperature);
    }
}
