package com.wegroszta.andrei.licenta.ui.settings;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.wegroszta.andrei.licenta.R;
import com.wegroszta.andrei.licenta.ui.auth.LoginActivity;
import com.wegroszta.andrei.licenta.ui.common.ViewModelFactory;
import com.wegroszta.andrei.licenta.viewmodels.auth.LoginViewModel;
import com.wegroszta.andrei.licenta.viewmodels.responses.Response;

public class SettingsFragment extends PreferenceFragmentCompat {

    private LoginViewModel loginViewModel;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        setupLoginViewModel();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreatePreferences(final Bundle savedInstanceState, final String rootKey) {
        addPreferencesFromResource(R.xml.app_preference);

        setupLogoutPref();
    }

    private void setupLogoutPref() {
        Preference logoutPref = findPreference("logout");
        logoutPref.setOnPreferenceClickListener(preference -> {
            loginViewModel.logout();
            return true;
        });
    }

    private void setupLoginViewModel() {
        if (getActivity() != null) {
            loginViewModel = ViewModelProviders.of(getActivity(), ViewModelFactory.getInstance())
                    .get(LoginViewModel.class);
            loginViewModel.getLogoutResponse().observe(this, this::processLogoutResponse);
        }
    }

    private void processLogoutResponse(final Response<Void> response) {
        switch (response.getState()) {
            case STARTED:
                break;
            case SUCCESS:
                break;
            case COMPLETED:
                logoutSuccess();
                break;
            case FAILURE:
                logoutFail(response.getError());
                break;
        }
    }

    private void logoutFail(final Throwable error) {
        error.printStackTrace();
        Toast.makeText(getActivity(), getText(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
    }

    private void logoutSuccess() {
        if (getActivity() != null) {
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finish();
        }
    }
}