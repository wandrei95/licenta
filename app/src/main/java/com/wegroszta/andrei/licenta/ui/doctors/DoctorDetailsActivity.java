package com.wegroszta.andrei.licenta.ui.doctors;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.wegroszta.andrei.licenta.R;
import com.wegroszta.andrei.licenta.entities.Doctor;
import com.wegroszta.andrei.licenta.ui.common.ViewModelFactory;
import com.wegroszta.andrei.licenta.viewmodels.doctors.DoctorCollaborationViewModel;
import com.wegroszta.andrei.licenta.viewmodels.doctors.DoctorsViewModel;
import com.wegroszta.andrei.licenta.viewmodels.responses.Response;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DoctorDetailsActivity extends AppCompatActivity {
    public static final String DOCTOR_ID_KEY = "doctorIdKey";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_first_name)
    TextView tvFirstName;
    @BindView(R.id.tv_last_name)
    TextView tvLastName;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.tv_phone_nr)
    TextView tvPhone;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_specialty)
    TextView tvSpecialty;
    @BindView(R.id.loading)
    ProgressBar loading;

    private DoctorCollaborationViewModel doctorCollaborationViewModel;
    private DoctorsViewModel doctorsViewModel;
    private String doctorId;
    private Menu menu;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_details);

        ButterKnife.bind(this);

        setActionBar();

        extractDataFromIntent();

        setupViewModels();

        fetchDoctor();
    }

    private void setActionBar() {
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_doctor_details, menu);
        doctorCollaborationViewModel.checkDoctorAssociationWithLoggedUser(doctorId);
        return super.onCreateOptionsMenu(menu);
    }

    private boolean doesIntentContainData() {
        return getIntent() != null && getIntent().getExtras() != null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_request_collab:
                doctorCollaborationViewModel.requestDoctorCollaboration(doctorId);
                return true;
            case R.id.action_finish_collab:
                doctorCollaborationViewModel.finishDoctorCollaboration(doctorId);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void fetchDoctor() {
        doctorsViewModel.getDoctor(doctorId);
    }

    private void extractDataFromIntent() {
        if (doesIntentContainData()) {
            extractDoctorIdFromBundle(Objects.requireNonNull(getIntent().getExtras()));
        }
    }

    private void extractDoctorIdFromBundle(final Bundle bundle) {
        if (bundle.containsKey(DOCTOR_ID_KEY)) {
            doctorId = bundle.getString(DOCTOR_ID_KEY);
        }
    }

    private void setupViewModels() {
        setupRequestDoctorCollaborationViewModel();
        setupGetDoctorsViewModel();
    }

    private void setupRequestDoctorCollaborationViewModel() {
        doctorCollaborationViewModel = ViewModelProviders.of(this, ViewModelFactory.getInstance())
                .get(DoctorCollaborationViewModel.class);
        doctorCollaborationViewModel.getRequestDoctorCollaborationResponse()
                .observe(this, this::processSendCollaborationResponse);
        doctorCollaborationViewModel.getFinishDoctorCollaborationResponse()
                .observe(this, this::processFinishCollaborationResponse);
        doctorCollaborationViewModel.getCheckDoctorAssociationResponse()
                .observe(this, this::processCheckDoctorAssociationResponse);
    }


    private void setupGetDoctorsViewModel() {
        doctorsViewModel = ViewModelProviders.of(this, ViewModelFactory.getInstance())
                .get(DoctorsViewModel.class);
        doctorsViewModel.getGetDoctorResponse()
                .observe(this, this::processGetDoctorResponse);
    }

    private void processSendCollaborationResponse(final Response<Void> response) {
        switch (response.getState()) {
            case STARTED:
                showLoadingState();
                break;
            case COMPLETED:
                onMessageSendSuccess();
                finish();
                break;
            case FAILURE:
                onSendRequestFail(response.getError());
                break;
        }
    }

    private void processFinishCollaborationResponse(final Response<Void> response) {
        switch (response.getState()) {
            case STARTED:
                showLoadingState();
                break;
            case COMPLETED:
                onMessageSendSuccess();
                finish();
                break;
            case FAILURE:
                onSendRequestFail(response.getError());
                break;
        }
    }

    private void processCheckDoctorAssociationResponse(Response<Boolean> booleanResponse) {
        switch (booleanResponse.getState()) {
            case STARTED:
                showLoadingState();
                break;
            case SUCCESS:
                if (booleanResponse.getData()) {
                    menu.findItem(R.id.action_finish_collab).setVisible(true);
                    menu.findItem(R.id.action_request_collab).setVisible(false);
                } else {
                    menu.findItem(R.id.action_request_collab).setVisible(true);
                    menu.findItem(R.id.action_finish_collab).setVisible(false);
                }
                break;
            case COMPLETED:
                onMessageSendSuccess();
                break;
            case FAILURE:
                onSendRequestFail(booleanResponse.getError());
                break;
        }
    }

    private void processGetDoctorResponse(final Response<Doctor> doctorResponse) {
        switch (doctorResponse.getState()) {
            case STARTED:
                showLoadingState();
                break;
            case SUCCESS:
                onDoctorDetailsLoaded(doctorResponse.getData());
                break;
            case COMPLETED:
                hideLoadingState();
                break;
            case FAILURE:
                onSendRequestFail(doctorResponse.getError());
                break;
        }
    }

    private void showLoadingState() {
        loading.setVisibility(View.VISIBLE);
    }

    private void hideLoadingState() {
        loading.setVisibility(View.GONE);
    }

    private void onMessageSendSuccess() {
        hideLoadingState();
    }

    private void onSendRequestFail(final Throwable throwable) {
        throwable.printStackTrace();
        hideLoadingState();
    }

    private void onDoctorDetailsLoaded(Doctor data) {
        tvFirstName.setText(data.getFirstName());
        tvLastName.setText(data.getLastName());
        tvEmail.setText(data.getEmail());
        tvPhone.setText(data.getPhone());
        tvAddress.setText(data.getAddress());
        tvSpecialty.setText(data.getSpecialty());
    }
}
