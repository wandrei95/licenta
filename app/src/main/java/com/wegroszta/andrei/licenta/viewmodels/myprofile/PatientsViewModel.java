package com.wegroszta.andrei.licenta.viewmodels.myprofile;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.wegroszta.andrei.licenta.entities.Patient;
import com.wegroszta.andrei.licenta.usecases.auth.UserDataFetcher;
import com.wegroszta.andrei.licenta.usecases.patients.PatientsInteractor;
import com.wegroszta.andrei.licenta.viewmodels.responses.Response;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class PatientsViewModel extends ViewModel {
    private final PatientsInteractor patientsInteractor;
    private final UserDataFetcher userDataFetcher;
    private final MutableLiveData<Response<Patient>> getPatientResponse;
    private final MutableLiveData<Response<Void>> savePatientResponse;
    private final CompositeDisposable disposables;

    public PatientsViewModel(final PatientsInteractor patientsInteractor,
                             final UserDataFetcher userDataFetcher) {
        this.patientsInteractor = patientsInteractor;
        this.userDataFetcher = userDataFetcher;
        getPatientResponse = new MutableLiveData<>();
        savePatientResponse = new MutableLiveData<>();
        disposables = new CompositeDisposable();
    }

    @Override
    protected void onCleared() {
        disposables.clear();
    }

    public MutableLiveData<Response<Patient>> getGetPatientResponse() {
        return getPatientResponse;
    }

    public MutableLiveData<Response<Void>> getSavePatientResponse() {
        return savePatientResponse;
    }

    public void savePatient(final Patient patient) {
        final String userId = userDataFetcher.getLoggedUserId();
        patient.setId(userId);
        disposables.add(patientsInteractor.savePatient(patient)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> savePatientResponse.setValue(Response.started()))
                .subscribe(
                        aVoid -> savePatientResponse.setValue(Response.success(aVoid)),
                        throwable -> savePatientResponse.setValue(Response.failure(throwable)),
                        () -> savePatientResponse.setValue(Response.completed(null))
                )
        );
    }

    public void getPatient() {
        final String userId = userDataFetcher.getLoggedUserId();
        disposables.add(patientsInteractor.getPatient(userId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getPatientResponse.setValue(Response.started()))
                .subscribe(
                        patient -> getPatientResponse.setValue(Response.success(patient)),
                        throwable -> getPatientResponse.setValue(Response.failure(throwable)),
                        () -> getPatientResponse.setValue(Response.completed(null))
                )
        );
    }
}
