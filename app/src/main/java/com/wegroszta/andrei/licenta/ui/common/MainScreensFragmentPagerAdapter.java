package com.wegroszta.andrei.licenta.ui.common;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.wegroszta.andrei.licenta.ui.doctors.DoctorsFragment;
import com.wegroszta.andrei.licenta.ui.measurement.MeasurementsFragment;
import com.wegroszta.andrei.licenta.ui.myprofile.MyProfileFragment;
import com.wegroszta.andrei.licenta.ui.settings.SettingsFragment;
import com.wegroszta.andrei.licenta.ui.statistics.StatisticsFragment;

import java.util.ArrayList;
import java.util.List;

public class MainScreensFragmentPagerAdapter extends FragmentPagerAdapter {
    private static final List<Fragment> fragments = new ArrayList<>();

    static {
        fragments.add(new MeasurementsFragment());
        fragments.add(new StatisticsFragment());
        fragments.add(new DoctorsFragment());
        fragments.add(new MyProfileFragment());
        fragments.add(new SettingsFragment());
    }

    MainScreensFragmentPagerAdapter(final FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(final int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
