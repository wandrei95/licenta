package com.wegroszta.andrei.licenta.io.measurement;

import com.wegroszta.andrei.licenta.entities.Temperature;
import com.wegroszta.andrei.licenta.usecases.measurement.temperature.TemperatureMeasurer;

import java.util.Random;

import io.reactivex.Observable;

public class DummyTemperatureMeasurer implements TemperatureMeasurer {
    @Override
    public Observable<Temperature> observeTemperature() {
        return Observable.create(e -> {
                    for (int i = 0; i < 10000; i++) {
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }

                        double random = new Random().nextDouble();
                        double min = 34d;
                        double max = 42d;
                        double value = min + (max - min) * random;
                        e.onNext(new Temperature(System.currentTimeMillis(), value));
                    }
                }
        );
    }

    @Override
    public void stopMeasuring() {

    }
}
