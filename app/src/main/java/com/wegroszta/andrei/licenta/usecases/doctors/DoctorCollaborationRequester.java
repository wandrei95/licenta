package com.wegroszta.andrei.licenta.usecases.doctors;

import io.reactivex.Observable;

public interface DoctorCollaborationRequester {
    Observable<Void> requestCollaboration(String userId, String userFirstName,
                                          String userLastName, String doctorId);

    Observable<Void> finishCollaboration(String userId, String doctorId);

    Observable<Boolean> checkDoctorAssociation(String patientId, String doctorId);
}
