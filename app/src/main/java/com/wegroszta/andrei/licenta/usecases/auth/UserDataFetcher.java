package com.wegroszta.andrei.licenta.usecases.auth;

public class UserDataFetcher {
    private final UserStorage userStorage;

    public UserDataFetcher(final UserStorage userStorage) {
        this.userStorage = userStorage;
    }

    public String getLoggedUserId() {
        return userStorage.getLoggedUserId();
    }
}
