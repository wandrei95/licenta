package com.wegroszta.andrei.licenta.usecases.auth;

import com.wegroszta.andrei.licenta.entities.Credentials;
import com.wegroszta.andrei.licenta.entities.User;

import io.reactivex.Observable;

public interface UserCreator {
    Observable<Void> signUp(Credentials credentials, User user);
}
