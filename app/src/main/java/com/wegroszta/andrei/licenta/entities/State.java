package com.wegroszta.andrei.licenta.entities;

public enum State {
    BELOW_AVG,
    AVG,
    ABOVE_AVG,
    ABNORMALITY
}
