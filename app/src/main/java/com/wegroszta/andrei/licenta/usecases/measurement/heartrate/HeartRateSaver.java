package com.wegroszta.andrei.licenta.usecases.measurement.heartrate;

import com.wegroszta.andrei.licenta.entities.HeartRate;

import io.reactivex.Observable;

public interface HeartRateSaver {
    Observable<Void> saveHeartRate(String userId, HeartRate heartRate);
}
