package com.wegroszta.andrei.licenta.usecases.measurement.heartrate;

import com.wegroszta.andrei.licenta.entities.HeartRate;

import io.reactivex.Observable;

public interface HeartRateMeasurer {
    Observable<HeartRate> observeHeartRate();

    void stopMeasuring();
}
