package com.wegroszta.andrei.licenta.ui.statistics;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;

import com.wegroszta.andrei.licenta.R;
import com.wegroszta.andrei.licenta.entities.HeartRate;
import com.wegroszta.andrei.licenta.ui.common.ViewModelFactory;
import com.wegroszta.andrei.licenta.viewmodels.responses.Response;
import com.wegroszta.andrei.licenta.viewmodels.statistics.FetchHeartRateStatisticsViewModel;

import java.util.List;

public class HeartRateStatisticsActivity extends StatisticsActivity {
    private FetchHeartRateStatisticsViewModel fetchHeartRateStatisticsViewModel;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupFetchHeartRateStatisticsViewModel();

        getData();
    }

    private void setupFetchHeartRateStatisticsViewModel() {
        fetchHeartRateStatisticsViewModel = ViewModelProviders.of(this, ViewModelFactory.getInstance())
                .get(FetchHeartRateStatisticsViewModel.class);
        fetchHeartRateStatisticsViewModel.getFetchHeartRateResponse()
                .observe(this, this::processHeartRateResponse);
    }

    private void processHeartRateResponse(final Response<List<HeartRate>> response) {
        switch (response.getState()) {
            case STARTED:
                showLoadingState();
                break;
            case SUCCESS:
                showGraph(response.getData());
                break;
            case COMPLETED:
                hideLoadingState();
                break;
            case FAILURE:
                onError(response.getError());
                break;
        }
    }

    @Override
    protected String getGraphTitle() {
        return getString(R.string.heart_rate);
    }

    private void getData() {
        fetchHeartRateStatisticsViewModel.getHeartRates();
    }

}
