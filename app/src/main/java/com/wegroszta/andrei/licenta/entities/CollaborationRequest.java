package com.wegroszta.andrei.licenta.entities;

public class CollaborationRequest {
    private final String id;
    private final String requesterFirstName;
    private final String requesterLastName;
    private final String requesterId;
    private final long timestmap;

    public CollaborationRequest() {
        id = null;
        requesterFirstName = null;
        requesterLastName = null;
        requesterId = null;
        timestmap = 0L;
    }

    public CollaborationRequest(final String id, final String requesterId, final String requesterFirstName,
                                final String requesterLastName, final long timestmap) {
        this.id = id;
        this.requesterFirstName = requesterFirstName;
        this.requesterLastName = requesterLastName;
        this.requesterId = requesterId;
        this.timestmap = timestmap;
    }

    public String getId() {
        return id;
    }

    public String getRequesterFirstName() {
        return requesterFirstName;
    }

    public String getRequesterLastName() {
        return requesterLastName;
    }

    public String getRequesterId() {
        return requesterId;
    }

    public long getTimestmap() {
        return timestmap;
    }
}
