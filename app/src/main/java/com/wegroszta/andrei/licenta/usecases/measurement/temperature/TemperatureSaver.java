package com.wegroszta.andrei.licenta.usecases.measurement.temperature;

import com.wegroszta.andrei.licenta.entities.Temperature;

import java.util.List;

import io.reactivex.Observable;

public interface TemperatureSaver {
    Observable<Void> saveTemperature(String userId, Temperature temperature);
}
