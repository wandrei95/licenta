package com.wegroszta.andrei.licenta.ui.doctors;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;

import com.wegroszta.andrei.licenta.R;
import com.wegroszta.andrei.licenta.entities.Doctor;
import com.wegroszta.andrei.licenta.ui.common.ViewModelFactory;
import com.wegroszta.andrei.licenta.viewmodels.doctors.DoctorsViewModel;
import com.wegroszta.andrei.licenta.viewmodels.responses.Response;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class DoctorsFragment extends Fragment implements DoctorsRecyclerViewAdapter.OnDoctorClickListener, SearchView.OnQueryTextListener, CompoundButton.OnCheckedChangeListener {
    @BindView(R.id.rv_doctors)
    RecyclerView rvDoctors;
    @BindView(R.id.switch_doctors)
    Switch switchDoctors;
    @BindView(R.id.search_view)
    SearchView searchView;
    @BindView(R.id.loading)
    ProgressBar loading;


    private Unbinder unbinder;
    private DoctorsRecyclerViewAdapter adapter;
    private DoctorsViewModel doctorsViewModel;

    public DoctorsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_doctors, container, false);

        unbinder = ButterKnife.bind(this, mainView);

        setup();
        fetchDoctors();

        return mainView;
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchDoctors();
    }

    private void fetchDoctors() {
        if (switchDoctors.isChecked()) {
            doctorsViewModel.getAllDoctors();
        } else {
            doctorsViewModel.getDoctorsForCurrentPatient();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void setup() {
        switchDoctors.setOnCheckedChangeListener(this);
        setupRecyclerView();
        setupSearchView();
        setupViewModels();
    }

    private void setupSearchView() {
        searchView.setOnQueryTextListener(this);
    }

    private void setupRecyclerView() {
        adapter = new DoctorsRecyclerViewAdapter();
        adapter.setOnDoctorClickListener(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvDoctors.setLayoutManager(layoutManager);
        rvDoctors.setAdapter(adapter);
    }

    private void setupViewModels() {
        setupGetDoctorsViewModel();
    }

    private void setupGetDoctorsViewModel() {
        doctorsViewModel = ViewModelProviders.of(this, ViewModelFactory.getInstance())
                .get(DoctorsViewModel.class);
        doctorsViewModel.getGetDoctorsResponse()
                .observe(this, this::processGetDoctorsResponse);
    }

    private void processGetDoctorsResponse(final Response<List<Doctor>> response) {
        switch (response.getState()) {
            case STARTED:
                showLoadingState();
                break;
            case SUCCESS:
                adapter.setDoctors(response.getData());
                break;
            case COMPLETED:
                hideLoadingState();
                break;
            case FAILURE:
                onGetDoctorsFail(response.getError());
                break;
        }
    }

    @Override
    public void onDoctorClicked(final Doctor doctor) {
        Intent intent = new Intent(getActivity(), DoctorDetailsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(DoctorDetailsActivity.DOCTOR_ID_KEY, doctor.getId());
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void showLoadingState() {
        loading.setVisibility(View.VISIBLE);
    }

    private void hideLoadingState() {
        loading.setVisibility(View.GONE);
    }

    private void onGetDoctorsFail(final Throwable throwable) {
        throwable.printStackTrace();
        hideLoadingState();
    }

    @Override
    public boolean onQueryTextSubmit(final String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(final String newText) {
        adapter.filter(newText);
        return false;
    }

    @Override
    public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
        fetchDoctors();
    }
}
