package com.wegroszta.andrei.licenta.viewmodels.measurement;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.wegroszta.andrei.licenta.entities.HeartRate;
import com.wegroszta.andrei.licenta.usecases.auth.UserDataFetcher;
import com.wegroszta.andrei.licenta.usecases.measurement.heartrate.MeasureHeartRate;
import com.wegroszta.andrei.licenta.viewmodels.responses.Response;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MeasureHeartRateViewModel extends ViewModel {
    private final MeasureHeartRate measureHeartRate;
    private final UserDataFetcher userDataFetcher;
    private final MutableLiveData<Response<HeartRate>> measureHeartRateResponse;
    private final MutableLiveData<Response<Void>> saveHeartRateResponse;
    private final CompositeDisposable disposables;

    public MeasureHeartRateViewModel(final MeasureHeartRate measureHeartRate,
                                     final UserDataFetcher userDataFetcher) {
        this.measureHeartRate = measureHeartRate;
        this.userDataFetcher = userDataFetcher;
        measureHeartRateResponse = new MutableLiveData<>();
        saveHeartRateResponse = new MutableLiveData<>();
        disposables = new CompositeDisposable();
    }

    @Override
    public void onCleared() {
        measureHeartRate.stopMeasuring();
        disposables.clear();
    }

    public MutableLiveData<Response<HeartRate>> getMeasureHeartRateResponse() {
        return measureHeartRateResponse;
    }

    public MutableLiveData<Response<Void>> getSaveHeartRateResponse() {
        return saveHeartRateResponse;
    }

    public void measureHeartRate() {
        disposables.add(measureHeartRate.observeHeartRate()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> measureHeartRateResponse.setValue(Response.started()))
                .subscribe(
                        heartRate -> measureHeartRateResponse.setValue(Response.success(heartRate)),
                        throwable -> measureHeartRateResponse.setValue(Response.failure(throwable)),
                        () -> measureHeartRateResponse.setValue(Response.completed(null))
                )
        );
    }

    public void saveHeartRate(final int value) {
        final String userId = userDataFetcher.getLoggedUserId();
        disposables.add(measureHeartRate.saveHeartRate(userId, value)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> saveHeartRateResponse.setValue(Response.started()))
                .subscribe(
                        aVoid -> saveHeartRateResponse.setValue(Response.success(aVoid)),
                        throwable -> saveHeartRateResponse.setValue(Response.failure(throwable)),
                        () -> saveHeartRateResponse.setValue(Response.completed(null))
                )
        );
    }
}
