package com.wegroszta.andrei.licenta.usecases.patients;

import com.wegroszta.andrei.licenta.entities.Patient;

import io.reactivex.Observable;

public class PatientsInteractor {
    private final PatientsStorage patientsStorage;

    public PatientsInteractor(final PatientsStorage patientsStorage) {
        this.patientsStorage = patientsStorage;
    }

    public Observable<Patient> getPatient(final String id) {
        return patientsStorage.getPatient(id);
    }

    public Observable<Void> savePatient(final Patient patient) {
        return patientsStorage.savePatient(patient);
    }
}
