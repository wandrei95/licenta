package com.wegroszta.andrei.licenta.entities;

public class HeartRate extends StateValueData {
    public HeartRate() {
        this(0L, State.AVG, 0);
    }

    public HeartRate(final long timestamp, final int value) {
        this(timestamp, State.AVG, value);
    }

    public HeartRate(final long timestamp, final State state, final int value) {
        super(timestamp, state, value);
    }
}
