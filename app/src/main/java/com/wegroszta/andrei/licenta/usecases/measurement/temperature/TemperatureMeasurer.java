package com.wegroszta.andrei.licenta.usecases.measurement.temperature;

import com.wegroszta.andrei.licenta.entities.Temperature;

import io.reactivex.Observable;

public interface TemperatureMeasurer {
    Observable<Temperature> observeTemperature();

    void stopMeasuring();
}
