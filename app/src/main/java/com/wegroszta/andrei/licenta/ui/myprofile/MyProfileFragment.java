package com.wegroszta.andrei.licenta.ui.myprofile;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.wegroszta.andrei.licenta.R;
import com.wegroszta.andrei.licenta.entities.Patient;
import com.wegroszta.andrei.licenta.ui.common.ViewModelFactory;
import com.wegroszta.andrei.licenta.ui.util.UITextUtil;
import com.wegroszta.andrei.licenta.viewmodels.myprofile.PatientsViewModel;
import com.wegroszta.andrei.licenta.viewmodels.responses.Response;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MyProfileFragment extends Fragment {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.et_first_name)
    EditText etFirstName;
    @BindView(R.id.et_last_name)
    EditText etLastName;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_phone_nr)
    EditText etPhone;
    @BindView(R.id.et_address)
    EditText etAddress;
    @BindView(R.id.loading)
    ProgressBar loading;

    private Unbinder unbinder;
    private PatientsViewModel patientsViewModel;

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_my_profile, container, false);

        unbinder = ButterKnife.bind(this, mainView);

        setActionBar();

        setupGetPatientsViewModel();
        getData();

        return mainView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_my_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void setActionBar() {
        toolbar.setTitle("");
        ((AppCompatActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save_my_profile:
                saveChanges();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void saveChanges() {
        Patient patient = getPatientFromFields();
        patientsViewModel.savePatient(patient);
    }

    @NonNull
    private Patient getPatientFromFields() {
        String firstName = etFirstName.getText().toString();
        String lastName = etLastName.getText().toString();
        String email = etEmail.getText().toString();
        String phone = etPhone.getText().toString();
        String address = etAddress.getText().toString();
        return new Patient(firstName, lastName, email, phone, address);
    }

    private void setupGetPatientsViewModel() {
        patientsViewModel = ViewModelProviders.of(this, ViewModelFactory.getInstance())
                .get(PatientsViewModel.class);
        patientsViewModel.getGetPatientResponse()
                .observe(this, this::processPatientResponse);
        patientsViewModel.getSavePatientResponse()
                .observe(this, this::processSavePatientResponse);
    }

    private void processPatientResponse(final Response<Patient> response) {
        switch (response.getState()) {
            case STARTED:
                showLoadingState();
                break;
            case SUCCESS:
                onPatientLoaded(response.getData());
                break;
            case COMPLETED:
                hideLoadingState();
                break;
            case FAILURE:
                onError(response.getError());
                break;
        }
    }

    private void processSavePatientResponse(final Response<Void> response) {
        switch (response.getState()) {
            case STARTED:
                showLoadingState();
                break;
            case SUCCESS:
                onPatientSaved();
                break;
            case COMPLETED:
                hideLoadingState();
                break;
            case FAILURE:
                onError(response.getError());
                break;
        }
    }

    private void onPatientSaved() {
        hideLoadingState();
        Toast.makeText(getActivity(), R.string.saving_data_success_msg, Toast.LENGTH_LONG).show();
    }

    private void onPatientLoaded(final Patient patient) {
        UITextUtil.setTextToEditText(etFirstName, patient.getFirstName());
        UITextUtil.setTextToEditText(etLastName, patient.getLastName());
        UITextUtil.setTextToEditText(etEmail, patient.getEmail());
        UITextUtil.setTextToEditText(etPhone, patient.getPhone());
        UITextUtil.setTextToEditText(etAddress, patient.getAddress());
    }

    private void showLoadingState() {
        loading.setVisibility(View.VISIBLE);
    }

    private void hideLoadingState() {
        loading.setVisibility(View.GONE);
    }

    private void onError(final Throwable throwable) {
        hideLoadingState();
        throwable.printStackTrace();
        Toast.makeText(getActivity(), R.string.something_went_wrong, Toast.LENGTH_LONG).show();
    }

    public void getData() {
        patientsViewModel.getPatient();
    }
}
