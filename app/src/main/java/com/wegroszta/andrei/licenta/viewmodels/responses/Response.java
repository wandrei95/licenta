package com.wegroszta.andrei.licenta.viewmodels.responses;

public class Response<T> {
    private final T data;
    private final State state;
    private final Throwable error;

    private Response(final T data, final State state, final Throwable error) {
        this.data = data;
        this.state = state;
        this.error = error;
    }

    public static <T> Response<T> started() {
        return new Response<>(null, State.STARTED, null);
    }

    public static <T> Response<T> success(final T data) {
        return new Response<>(data, State.SUCCESS, null);
    }

    public static <T> Response<T> completed(final T data) {
        return new Response<>(data, State.COMPLETED, null);
    }

    public static <T> Response<T> failure(final Throwable e) {
        return new Response<>(null, State.FAILURE, e);
    }

    public T getData() {
        return data;
    }

    public State getState() {
        return state;
    }

    public Throwable getError() {
        return error;
    }
}
