package com.wegroszta.andrei.licenta.entities;

public class Data {
    private long timestamp;

    public Data() {
        this(0L);
    }

    public Data(final long timestamp) {
        this.timestamp = timestamp;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final long timestamp) {
        this.timestamp = timestamp;
    }
}
