package com.wegroszta.andrei.licenta.entities;

public class Temperature extends StateValueData {
    public Temperature() {
        this(0L, State.AVG, 0d);
    }

    public Temperature(final long timestamp, final double value) {
        this(timestamp, State.AVG, value);
    }

    public Temperature(final long timestamp, final State state, final double value) {
        super(timestamp, state, value);
    }
}