package com.wegroszta.andrei.licenta.usecases.auth;

import com.wegroszta.andrei.licenta.entities.Credentials;

import io.reactivex.Observable;

public interface Authenticator {
    Observable<Void> authenticate(Credentials credentials);

    Observable<Void> logout();

    boolean isAnyUserLogged();
}
