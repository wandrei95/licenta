package com.wegroszta.andrei.licenta.ui.common;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.wegroszta.andrei.licenta.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class OptionsView extends FrameLayout implements View.OnClickListener {
    @BindView(R.id.temperature_container)
    RelativeLayout temperatureContainer;
    @BindView(R.id.heart_rate_container)
    RelativeLayout heartRateContainer;

    private OnOptionClickedListener onOptionClickedListener;
    private Unbinder unbinder;

    public OptionsView(final Context context) {
        super(context);
        init();
    }

    public OptionsView(final Context context, @Nullable final AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public OptionsView(final Context context, @Nullable final AttributeSet attrs,
                       final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public OptionsView(final Context context, @Nullable final AttributeSet attrs,
                       final int defStyleAttr, final int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.options_view, this, true);

        unbinder = ButterKnife.bind(this);

        temperatureContainer.setOnClickListener(this);
        heartRateContainer.setOnClickListener(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        unbinder.unbind();
    }

    public void setOnOptionClickedListener(final OnOptionClickedListener onOptionClickedListener) {
        this.onOptionClickedListener = onOptionClickedListener;
    }

    @Override
    public void onClick(final View view) {
        Option option = null;
        switch (view.getId()) {
            case R.id.temperature_container:
                option = Option.TEMPERATURE;
                break;
            case R.id.heart_rate_container:
                option = Option.HEART_RATE;
                break;
        }

        if ((option != null) && (onOptionClickedListener != null)) {
            onOptionClickedListener.onOptionClicked(option);
        }
    }

    public interface OnOptionClickedListener {
        void onOptionClicked(Option option);
    }

    public enum Option {
        TEMPERATURE,
        HEART_RATE,
    }
}
