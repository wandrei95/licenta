package com.wegroszta.andrei.licenta.usecases.statistics.temperature;

import com.wegroszta.andrei.licenta.entities.Temperature;

import java.util.List;

import io.reactivex.Observable;

public interface TemperatureStatisticsFetcher {
    Observable<List<Temperature>> getTemperatures(String userId);
}
