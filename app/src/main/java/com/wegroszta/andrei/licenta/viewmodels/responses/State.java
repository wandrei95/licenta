package com.wegroszta.andrei.licenta.viewmodels.responses;

public enum State {
    STARTED,
    SUCCESS,
    COMPLETED,
    FAILURE
}
