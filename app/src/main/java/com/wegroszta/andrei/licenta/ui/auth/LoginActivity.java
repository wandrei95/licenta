package com.wegroszta.andrei.licenta.ui.auth;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.wegroszta.andrei.licenta.R;
import com.wegroszta.andrei.licenta.ui.common.MainActivity;
import com.wegroszta.andrei.licenta.ui.util.ValidationUtil;
import com.wegroszta.andrei.licenta.ui.common.ViewModelFactory;
import com.wegroszta.andrei.licenta.viewmodels.auth.LoginViewModel;
import com.wegroszta.andrei.licenta.viewmodels.responses.Response;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.loading)
    ProgressBar loading;

    private LoginViewModel loginViewModel;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        setClickListeners();

        setupLoginViewModel();

        if (loginViewModel.isUserLoggedIn()) {
            loginSuccess();
        }
    }

    private void setClickListeners() {
        findViewById(R.id.tv_sign_up).setOnClickListener(this);
        findViewById(R.id.btn_login).setOnClickListener(this);
    }

    @Override
    public void onClick(final View view) {
        switch (view.getId()) {
            case R.id.tv_sign_up:
                goToSignUp();
                break;
            case R.id.btn_login:
                userLogin();
                break;
        }
    }

    private void setupLoginViewModel() {
        loginViewModel = ViewModelProviders.of(this, ViewModelFactory.getInstance())
                .get(LoginViewModel.class);
        loginViewModel.getLoginResponse().observe(this, this::processLoginResponse);
    }

    private void processLoginResponse(final Response<Void> loginResponse) {
        switch (loginResponse.getState()) {
            case STARTED:
                showLoadingState();
                break;
            case SUCCESS:
                break;
            case COMPLETED:
                loginSuccess();
                break;
            case FAILURE:
                loginFail(loginResponse.getError());
                break;
        }
    }

    private void goToSignUp() {
        startActivity(new Intent(this, SignUpActivity.class));
    }

    private void goToMain() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    private void hideLoadingState() {
        loading.setVisibility(View.GONE);
    }

    private void showLoadingState() {
        loading.setVisibility(View.VISIBLE);
    }

    private void userLogin() {
        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        if (areCredentialsValid(email, password)) {
            loginViewModel.login(email, password);
        }
    }

    private boolean areCredentialsValid(final String email, final String password) {
        return !(isEmailEmpty(email) && isEmailFormatInvalid(email)
                && isPasswordEmpty(password) && isPasswordLengthInvalid(password));
    }

    private boolean isEmailEmpty(final String email) {
        if (ValidationUtil.isStringNullOrEmpty(email)) {
            etEmail.setError(getString(R.string.empty_email_msg));
            etEmail.requestFocus();
            return true;
        }
        return false;
    }

    private boolean isEmailFormatInvalid(final String email) {
        if (ValidationUtil.isEmailFormatInvalid(email)) {
            etEmail.setError(getString(R.string.invalid_email_format_msg));
            etEmail.requestFocus();
            return true;
        }
        return false;
    }

    private boolean isPasswordEmpty(final String password) {
        if (ValidationUtil.isStringNullOrEmpty(password)) {
            etPassword.setError(getString(R.string.empty_password_msg));
            etPassword.requestFocus();
            return true;
        }
        return false;
    }

    private boolean isPasswordLengthInvalid(final String password) {
        if (ValidationUtil.isPasswordLengthInvalid(password)) {
            etPassword.setError(getString(R.string.too_short_password_msg));
            etPassword.requestFocus();
            return true;
        }
        return false;
    }

    private void loginFail(final Throwable throwable) {
        hideLoadingState();
        Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
    }

    private void loginSuccess() {
        hideLoadingState();
        goToMain();
    }
}