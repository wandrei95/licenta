package com.wegroszta.andrei.licenta;

import android.app.Application;

import com.wegroszta.andrei.licenta.ui.util.SettingsPreferencesUtil;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        SettingsPreferencesUtil.setContext(this);
    }
}
