package com.wegroszta.andrei.licenta.usecases.auth;

import com.wegroszta.andrei.licenta.entities.Credentials;
import com.wegroszta.andrei.licenta.entities.User;

import io.reactivex.Observable;

public class Login {
    private final Authenticator authenticator;
    private final UserCreator userCreator;

    public Login(final Authenticator authenticator, final UserCreator userCreator) {
        this.authenticator = authenticator;
        this.userCreator = userCreator;
    }

    public Observable<Void> login(final Credentials credentials) {
        return authenticator.authenticate(credentials);
    }

    public Observable<Void> logout() {
        return authenticator.logout();
    }

    public Observable<Void> signUp(final Credentials credentials, final User user) {
        return userCreator.signUp(credentials, user);
    }

    public boolean isUserLoggedIn() {
        return authenticator.isAnyUserLogged();
    }
}
