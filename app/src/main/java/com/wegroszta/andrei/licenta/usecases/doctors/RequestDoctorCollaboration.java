package com.wegroszta.andrei.licenta.usecases.doctors;

import io.reactivex.Observable;

public class RequestDoctorCollaboration {
    private final DoctorCollaborationRequester doctorCollaborationRequester;

    public RequestDoctorCollaboration(final DoctorCollaborationRequester doctorCollaborationRequester) {
        this.doctorCollaborationRequester = doctorCollaborationRequester;
    }

    public Observable<Void> requestCollaboration(final String userId, final String userFirstName,
                                                 final String userLastName, final String doctorId) {
        return doctorCollaborationRequester.requestCollaboration(userId, userFirstName, userLastName, doctorId);
    }

    public Observable<Void> finishCollaboration(final String userId, final String doctorId) {
        return doctorCollaborationRequester.finishCollaboration(userId, doctorId);
    }

    public Observable<Boolean> checkDoctorAssociation(String patientId, String doctorId) {
        return doctorCollaborationRequester.checkDoctorAssociation(patientId, doctorId);
    }
}
