package com.wegroszta.andrei.licenta.ui.statistics;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wegroszta.andrei.licenta.R;
import com.wegroszta.andrei.licenta.ui.common.OptionsView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class StatisticsFragment extends Fragment implements OptionsView.OnOptionClickedListener {
    @BindView(R.id.options_view)
    OptionsView optionsView;

    private Unbinder unbinder;

    public StatisticsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_measurements, container, false);

        unbinder = ButterKnife.bind(this, view);
        optionsView.setOnOptionClickedListener(this);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onOptionClicked(final OptionsView.Option option) {
        switch (option) {
            case TEMPERATURE:
                goToTemperatureStatistics();
                break;
            case HEART_RATE:
                goToHeartRateStatistics();
                break;
        }
    }

    private void goToTemperatureStatistics() {
        startActivity(new Intent(getActivity(), TemperatureStatisticsActivity.class));
    }

    private void goToHeartRateStatistics() {
        startActivity(new Intent(getActivity(), HeartRateStatisticsActivity.class));
    }
}
