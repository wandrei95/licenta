package com.wegroszta.andrei.licenta.usecases.patients;

import com.wegroszta.andrei.licenta.entities.Patient;

import io.reactivex.Observable;

public interface PatientsStorage {
    Observable<Patient> getPatient(String id);

    Observable<Void> savePatient(Patient patient);
}
