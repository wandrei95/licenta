package com.wegroszta.andrei.licenta.io.doctors;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.wegroszta.andrei.licenta.entities.CollaborationRequest;
import com.wegroszta.andrei.licenta.entities.Doctor;
import com.wegroszta.andrei.licenta.usecases.doctors.DoctorCollaborationRequester;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;

public class FirebaseDoctorCollaborationRequester implements DoctorCollaborationRequester {
    private final DatabaseReference baseDatabaseReference;

    public FirebaseDoctorCollaborationRequester(final DatabaseReference baseDatabaseReference) {
        this.baseDatabaseReference = baseDatabaseReference;
    }

    @Override
    public Observable<Void> requestCollaboration(final String userId, final String userFirstName,
                                                 final String userLastName, final String doctorId) {
        return Observable.create(source -> {
            saveRequest(userId, userFirstName, userLastName, doctorId, source);
        });
    }

    @Override
    public Observable<Void> finishCollaboration(final String userId, final String doctorId) {
        return Observable.create(source -> {
            finishCollaboration(userId, doctorId, source);
        });
    }

    @Override
    public Observable<Boolean> checkDoctorAssociation(String patientId, String doctorId) {
        return Observable.create(source -> checkDoctorAssociation(source, patientId, doctorId));
    }

    private void saveRequest(final String userId, final String userFirstName, final String userLastName,
                             final String doctorId, final ObservableEmitter<Void> source) {
        DatabaseReference databaseReference = getCollaborationRequestsDatabaseRefForDoctor(doctorId, userId);
        long timestamp = System.currentTimeMillis();
        CollaborationRequest collaborationRequest = new CollaborationRequest(userId,
                userId, userFirstName, userLastName, timestamp);
        databaseReference
                .setValue(collaborationRequest)
                .addOnCompleteListener(task -> source.onComplete())
                .addOnFailureListener(source::onError);
    }

    private DatabaseReference getCollaborationRequestsDatabaseRefForDoctor(
            final String doctorId, final String patientId) {
        return baseDatabaseReference.child("doctors")
                .child(doctorId)
                .child("collaborationRequests")
                .child(patientId);
    }

    private void finishCollaboration(final String userId, final String doctorId,
                                     final ObservableEmitter<Void> source) {
        DatabaseReference databaseReference = getMyDoctorDatabaseReference(doctorId, userId);

        databaseReference.removeValue((databaseError, databaseReference1) -> {
            if (databaseError == null) {
                source.onComplete();
            } else {
                source.onError(databaseError.toException());
            }
        });
    }

    private DatabaseReference getMyDoctorDatabaseReference(final String doctorId,
                                                           final String patientId) {
        return baseDatabaseReference.child("patients")
                .child(patientId)
                .child("myDoctors")
                .child(doctorId);
    }

    private void checkDoctorAssociation(ObservableEmitter<Boolean> source, String patientId, String doctorId) {
        DatabaseReference databaseReference = getMyDoctorDatabaseReference(doctorId, patientId);
        databaseReference.addListenerForSingleValueEvent(createValueEventListenerForDoctorAssociation(source));
    }

    private ValueEventListener createValueEventListenerForDoctorAssociation(final ObservableEmitter<Boolean> source) {
        return new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                Doctor doctor = dataSnapshot.getValue(Doctor.class);
                if (doctor != null) {
                    source.onNext(true);
                } else {
                    source.onNext(false);
                }
                source.onComplete();
            }

            @Override
            public void onCancelled(final DatabaseError databaseError) {
                source.onError(databaseError.toException());
            }
        };
    }
}
