package com.wegroszta.andrei.licenta.usecases.statistics.heartrate;

import com.wegroszta.andrei.licenta.entities.HeartRate;

import java.util.List;

import io.reactivex.Observable;

public interface HeartRateStatisticsFetcher {
    Observable<List<HeartRate>> getHeartRate(String userId);
}
