package com.wegroszta.andrei.licenta.io.measurement;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import com.wegroszta.andrei.licenta.entities.Temperature;
import com.wegroszta.andrei.licenta.usecases.measurement.temperature.TemperatureMeasurer;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;

public class BluetoothTemperatureMeasurer implements TemperatureMeasurer {
    private static final String TEMP_REQUEST_KEY = "temp";

    private boolean shouldReadTemp;

    @Override
    public Observable<Temperature> observeTemperature() {
        return Observable.create(source -> {
                    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                    BluetoothDevice bluetoothDevice = getBluetoothDevice(mBluetoothAdapter);
                    if (bluetoothDevice == null) {
                        source.onError(new Exception("Could not find any bounded device"));
                        return;
                    }

                    final BluetoothSocket bluetoothSocket = createBluetoothDevice(bluetoothDevice);

                    try {
                        connectBluetoothSocket(bluetoothSocket);
                        sendTemperatureRequest(bluetoothSocket);
                    } catch (IOException e) {
                        source.onError(new Exception("Could not send temperature request"));
                        return;
                    }

                    try {
                        shouldReadTemp = true;
                        readTemperature(source, bluetoothSocket);
                        bluetoothSocket.close();
                    } catch (IOException e) {
                        source.onError(new Exception("An error has occurred while reading the temperature"));
                    }
                }
        );
    }

    @Override
    public void stopMeasuring() {
        shouldReadTemp = false;
    }

    private BluetoothSocket createBluetoothDevice(
            final BluetoothDevice bluetoothDevice) throws IOException {
        UUID uuid = UUID.fromString("94f39d29-7d6d-437d-973b-fba39e49d4ee"); //Standard SerialPortService ID
        return bluetoothDevice.createRfcommSocketToServiceRecord(uuid);
    }

    private void readTemperature(final ObservableEmitter<Temperature> source,
                                 final BluetoothSocket bluetoothSocket) throws IOException {
        final InputStream inputStream = bluetoothSocket.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        while (shouldReadTemp) {
            String msg = reader.readLine();
            Log.e("TemperatureMeasurer", msg);
            try {
                JSONObject jsonObject = new JSONObject(msg);
                long timestamp = jsonObject.getLong("timestamp");
                double temp = jsonObject.getDouble("temp_c");
                source.onNext(new Temperature(timestamp, temp));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        reader.close();
        bluetoothSocket.close();
    }

    private void sendTemperatureRequest(final BluetoothSocket bluetoothSocket) throws IOException {
        OutputStream outputStream = bluetoothSocket.getOutputStream();
        outputStream.write(TEMP_REQUEST_KEY.getBytes());
    }

    private BluetoothDevice getBluetoothDevice(final BluetoothAdapter mBluetoothAdapter) {
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                if (device.getName().equals("raspberrypi")) {
                    return device;
                }
            }
        }
        return null;
    }

    private void connectBluetoothSocket(final BluetoothSocket bluetoothSocket) throws IOException {
        if (!bluetoothSocket.isConnected()) {
            bluetoothSocket.connect();
        }
    }
}
