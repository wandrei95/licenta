package com.wegroszta.andrei.licenta.io.auth;

import com.google.firebase.auth.FirebaseAuth;
import com.wegroszta.andrei.licenta.entities.Credentials;
import com.wegroszta.andrei.licenta.usecases.auth.Authenticator;

import io.reactivex.Observable;

public class FirebaseAuthenticator implements Authenticator {
    private final FirebaseAuth firebaseAuth;

    public FirebaseAuthenticator(final FirebaseAuth firebaseAuth) {
        this.firebaseAuth = firebaseAuth;
    }

    @Override
    public Observable<Void> authenticate(final Credentials credentials) {
        return Observable.create(e -> firebaseAuth.signInWithEmailAndPassword(credentials.getEmail(), credentials.getPassword())
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        e.onComplete();
                    } else {
                        e.onError(new RuntimeException("Sign Up Failed"));
                    }
                }));
    }

    @Override
    public Observable<Void> logout() {
        return Observable.create(e -> {
            firebaseAuth.signOut();
            e.onComplete();
        });
    }

    @Override
    public boolean isAnyUserLogged() {
        return firebaseAuth.getCurrentUser() != null;
    }
}
