package com.wegroszta.andrei.licenta.ui.measurement;

import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wegroszta.andrei.licenta.R;
import com.wegroszta.andrei.licenta.entities.HeartRate;
import com.wegroszta.andrei.licenta.ui.common.ViewModelFactory;
import com.wegroszta.andrei.licenta.viewmodels.measurement.MeasureHeartRateViewModel;
import com.wegroszta.andrei.licenta.viewmodels.responses.Response;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HeartRateMeasurementActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String HEART_RATE_FORMAT = "%3d";

    @BindView(R.id.tv_heart_rate)
    TextView tvHeartRate;
    @BindView(R.id.cv_start)
    CardView cvStartMeasure;
    @BindView(R.id.rl_start)
    RelativeLayout rlStart;
    @BindView(R.id.cv_stop)
    CardView cvStopMeasure;
    @BindView(R.id.rl_stop)
    RelativeLayout rlStop;
    @BindView(R.id.cv_save_data)
    CardView cvSaveData;
    @BindView(R.id.rl_save_data)
    RelativeLayout rlSaveData;
    @BindView(R.id.loading)
    ProgressBar loading;

    private MeasureHeartRateViewModel measureHeartRateViewModel;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heart_rate_measurement);

        ButterKnife.bind(this);
        setClickListeners();

        setupMeasureHeartRateViewModel();
    }

    private void setClickListeners() {
        rlStart.setOnClickListener(this);
        rlStop.setOnClickListener(this);
        rlSaveData.setOnClickListener(this);
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.rl_start:
                startMeasure();
                break;
            case R.id.rl_stop:
                stopMeasure();
                break;
            case R.id.rl_save_data:
                saveHeartRate();
                break;
        }
    }

    private void setupMeasureHeartRateViewModel() {
        measureHeartRateViewModel = ViewModelProviders.of(this, ViewModelFactory.getInstance())
                .get(MeasureHeartRateViewModel.class);
        measureHeartRateViewModel.getMeasureHeartRateResponse().observe(this,
                this::processMeasureHeartRateResponse);
        measureHeartRateViewModel.getSaveHeartRateResponse().observe(this,
                this::processSaveHeartRateResponse);
    }

    private void processMeasureHeartRateResponse(final Response<HeartRate> heartRateResponse) {
        switch (heartRateResponse.getState()) {
            case STARTED:
                enterStartMeasureState();
                break;
            case SUCCESS:
                onHeartRateFetched(heartRateResponse.getData());
                break;
            case FAILURE:
                onMeasureHeartRateFailure(heartRateResponse.getError());
                break;
        }
    }


    private void processSaveHeartRateResponse(final Response<Void> response) {
        switch (response.getState()) {
            case STARTED:
                showLoadingState();
                break;
            case COMPLETED:
                onSaveDataSuccess();
                break;
            case FAILURE:
                onSaveDataFailed(response.getError());
                break;
        }
    }

    private void stopMeasure() {
        enterStopMeasureState();
        measureHeartRateViewModel.onCleared();
    }

    private void startMeasure() {
        enterStartMeasureState();
        measureHeartRateViewModel.measureHeartRate();
    }

    private void showLoadingState() {
        loading.setVisibility(View.VISIBLE);
    }

    private void hideLoadingState() {
        loading.setVisibility(View.GONE);
    }

    private void enterStartMeasureState() {
        showLoadingState();
        cvStartMeasure.setVisibility(View.GONE);
        cvStopMeasure.setVisibility(View.VISIBLE);
    }

    private void enterStopMeasureState() {
        hideLoadingState();
        cvStartMeasure.setVisibility(View.VISIBLE);
        cvStopMeasure.setVisibility(View.GONE);
    }

    private void saveHeartRate() {
        measureHeartRateViewModel.saveHeartRate(Integer.parseInt(tvHeartRate.getText().toString().trim()));
    }

    private void onSaveDataSuccess() {
        hideLoadingState();
        Toast.makeText(this, R.string.saving_data_success_msg, Toast.LENGTH_LONG).show();
    }

    private void onSaveDataFailed(final Throwable cause) {
        cause.printStackTrace();
        hideLoadingState();
        Toast.makeText(this, R.string.saving_data_error_msg, Toast.LENGTH_LONG).show();
    }

    private void onHeartRateFetched(final HeartRate heartRate) {
        hideLoadingState();
        if (heartRate.getValue() > 0) {
            showHeartRateDataOnScreen(heartRate);
            cvSaveData.setVisibility(View.VISIBLE);
        }
    }

    private void onMeasureHeartRateFailure(Throwable error) {
        hideLoadingState();
        Toast.makeText(this, error.getMessage(), Toast.LENGTH_LONG).show();
    }

    private void showHeartRateDataOnScreen(final HeartRate heartRate) {
        tvHeartRate.setText(String.format(Locale.US, HEART_RATE_FORMAT, (int) heartRate.getValue()));
        setHeartRateStateFields(heartRate);
    }

    private void setHeartRateStateFields(final HeartRate heartRate) {
        switch (heartRate.getState()) {
            case BELOW_AVG:
                enterTooLowHeartRateState();
                break;
            case AVG:
                enterNormalHeartRateState();
                break;
            case ABOVE_AVG:
                enterTooHighHeartRateState();
                break;
        }
    }

    private void enterTooLowHeartRateState() {
        tvHeartRate.setTextColor(Color.MAGENTA);
    }

    private void enterNormalHeartRateState() {
        tvHeartRate.setTextColor(Color.WHITE);
    }

    private void enterTooHighHeartRateState() {
        tvHeartRate.setTextColor(Color.RED);
    }
}
