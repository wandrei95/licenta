package com.wegroszta.andrei.licenta.ui.statistics;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;

import com.wegroszta.andrei.licenta.R;
import com.wegroszta.andrei.licenta.entities.Temperature;
import com.wegroszta.andrei.licenta.ui.common.ViewModelFactory;
import com.wegroszta.andrei.licenta.viewmodels.responses.Response;
import com.wegroszta.andrei.licenta.viewmodels.statistics.FetchTemperatureStatisticsViewModel;

import java.util.List;

public class TemperatureStatisticsActivity extends StatisticsActivity {
    private FetchTemperatureStatisticsViewModel fetchTemperatureStatisticsViewModel;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupFetchTemperatureStatisticsViewModel();

        getData();
    }

    private void setupFetchTemperatureStatisticsViewModel() {
        fetchTemperatureStatisticsViewModel = ViewModelProviders.of(this, ViewModelFactory.getInstance())
                .get(FetchTemperatureStatisticsViewModel.class);
        fetchTemperatureStatisticsViewModel.getFetchTemperatureResponse()
                .observe(this, this::processHeartRateResponse);
    }

    private void processHeartRateResponse(final Response<List<Temperature>> response) {
        switch (response.getState()) {
            case STARTED:
                showLoadingState();
                break;
            case SUCCESS:
                showGraph(response.getData());
                break;
            case COMPLETED:
                hideLoadingState();
                break;
            case FAILURE:
                onError(response.getError());
                break;
        }
    }

    @Override
    protected String getGraphTitle() {
        return getString(R.string.temperature);
    }

    private void getData() {
        fetchTemperatureStatisticsViewModel.getTemperatures();
    }
}
