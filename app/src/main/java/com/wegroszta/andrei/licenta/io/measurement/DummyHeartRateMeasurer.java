package com.wegroszta.andrei.licenta.io.measurement;

import com.wegroszta.andrei.licenta.entities.HeartRate;
import com.wegroszta.andrei.licenta.usecases.measurement.heartrate.HeartRateMeasurer;

import java.util.Random;

import io.reactivex.Observable;

public class DummyHeartRateMeasurer implements HeartRateMeasurer {
    @Override
    public Observable<HeartRate> observeHeartRate() {
        return Observable.create(e -> {
                    for (int i = 0; i < 10000; i++) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }

                        int min = 40;
                        int max = 120;
                        int value = new Random().nextInt((max - min) + 1) + min;
                        e.onNext(new HeartRate(System.currentTimeMillis(), value));
                    }
                }
        );
    }

    @Override
    public void stopMeasuring() {

    }
}
