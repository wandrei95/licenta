package com.wegroszta.andrei.licenta.ui.measurement;

import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wegroszta.andrei.licenta.R;
import com.wegroszta.andrei.licenta.entities.Temperature;
import com.wegroszta.andrei.licenta.ui.common.ViewModelFactory;
import com.wegroszta.andrei.licenta.viewmodels.measurement.MeasureTemperatureViewModel;
import com.wegroszta.andrei.licenta.viewmodels.responses.Response;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TemperatureMeasurementActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TEMPERATURE_FORMAT = "%2.1f";

    @BindView(R.id.tv_temperature)
    TextView tvTemperature;
    @BindView(R.id.cv_start)
    CardView cvStartMeasure;
    @BindView(R.id.rl_start)
    RelativeLayout rlStart;
    @BindView(R.id.cv_stop)
    CardView cvStopMeasure;
    @BindView(R.id.rl_stop)
    RelativeLayout rlStop;
    @BindView(R.id.cv_save_data)
    CardView cvSaveData;
    @BindView(R.id.rl_save_data)
    RelativeLayout rlSaveData;
    @BindView(R.id.loading)
    ProgressBar loading;

    private MeasureTemperatureViewModel measureTemperatureViewModel;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature_measurement);

        ButterKnife.bind(this);
        setClickListeners();

        setupMeasureTemperatureViewModel();
    }

    private void setClickListeners() {
        rlStart.setOnClickListener(this);
        rlStop.setOnClickListener(this);
        rlSaveData.setOnClickListener(this);
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.rl_start:
                startMeasure();
                break;
            case R.id.rl_stop:
                stopMeasure();
                break;
            case R.id.rl_save_data:
                saveTemperature();
                break;
        }
    }

    private void setupMeasureTemperatureViewModel() {
        measureTemperatureViewModel = ViewModelProviders.of(this, ViewModelFactory.getInstance())
                .get(MeasureTemperatureViewModel.class);
        measureTemperatureViewModel.getMeasureTemperatureResponse().observe(this,
                this::processMeasureTemperatureResponse);
        measureTemperatureViewModel.getSaveTemperatureResponse().observe(this,
                this::processSaveTemperatureResponse);
    }

    private void processMeasureTemperatureResponse(final Response<Temperature> temperatureResponse) {
        switch (temperatureResponse.getState()) {
            case STARTED:
                enterStartMeasureState();
                break;
            case SUCCESS:
                onTemperatureFetched(temperatureResponse.getData());
                break;
            case FAILURE:
                onMeasureTemperatureFailure(temperatureResponse.getError());
                break;
        }
    }

    private void processSaveTemperatureResponse(final Response<Void> response) {
        switch (response.getState()) {
            case STARTED:
                showLoadingState();
                break;
            case COMPLETED:
                onSaveDataSuccess();
                break;
            case FAILURE:
                onSaveDataFailed(response.getError());
                break;
        }
    }

    private void stopMeasure() {
        enterStopMeasureState();
        measureTemperatureViewModel.onCleared();
    }

    private void startMeasure() {
        enterStartMeasureState();
        measureTemperatureViewModel.measureTemperature();
    }

    private void showLoadingState() {
        loading.setVisibility(View.VISIBLE);
    }

    private void hideLoadingState() {
        loading.setVisibility(View.GONE);
    }

    private void enterStartMeasureState() {
        showLoadingState();
        cvStartMeasure.setVisibility(View.GONE);
        cvStopMeasure.setVisibility(View.VISIBLE);
    }

    private void enterStopMeasureState() {
        cvStartMeasure.setVisibility(View.VISIBLE);
        cvStopMeasure.setVisibility(View.GONE);
    }

    private void saveTemperature() {
        measureTemperatureViewModel.saveTemperature(Double.parseDouble(tvTemperature.getText().toString()));
    }

    private void onSaveDataSuccess() {
        hideLoadingState();
        Toast.makeText(this, R.string.saving_data_success_msg, Toast.LENGTH_LONG).show();
    }

    private void onSaveDataFailed(final Throwable cause) {
        cause.printStackTrace();
        hideLoadingState();
        Toast.makeText(this, R.string.saving_data_error_msg, Toast.LENGTH_LONG).show();
    }

    private void onTemperatureFetched(final Temperature temperature) {
        hideLoadingState();
        cvSaveData.setVisibility(View.VISIBLE);
        showTemperatureDataOnScreen(temperature);
    }

    private void onMeasureTemperatureFailure(Throwable error) {
        hideLoadingState();
        Toast.makeText(this, error.getMessage(), Toast.LENGTH_LONG).show();
    }

    private void showTemperatureDataOnScreen(final Temperature temperature) {
        tvTemperature.setText(String.format(Locale.US, TEMPERATURE_FORMAT, temperature.getValue()));
        setTemperatureStateFields(temperature);
    }

    private void setTemperatureStateFields(final Temperature temperature) {
        switch (temperature.getState()) {
            case BELOW_AVG:
                enterTooLowTemperatureState();
                break;
            case AVG:
                enterNormalTemperatureState();
                break;
            case ABOVE_AVG:
                enterTooHighTemperatureState();
                break;
        }
    }

    private void enterTooLowTemperatureState() {
        tvTemperature.setTextColor(Color.MAGENTA);
    }

    private void enterNormalTemperatureState() {
        tvTemperature.setTextColor(Color.WHITE);
    }

    private void enterTooHighTemperatureState() {
        tvTemperature.setTextColor(Color.RED);
    }
}
