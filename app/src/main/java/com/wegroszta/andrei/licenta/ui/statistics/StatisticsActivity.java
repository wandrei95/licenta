package com.wegroszta.andrei.licenta.ui.statistics;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.BaseSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.PointsGraphSeries;
import com.wegroszta.andrei.licenta.R;
import com.wegroszta.andrei.licenta.entities.StateValueData;
import com.wegroszta.andrei.licenta.ui.common.StateValueDataPoint;
import com.wegroszta.andrei.licenta.ui.util.SettingsPreferencesUtil;

import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("Registered")
public abstract class StatisticsActivity extends AppCompatActivity {
    private static final int MAX_ITEMS_IN_VIEWPORT = 10;
    private static final int SERIES_ITEMS_SPACING = 5;

    @BindView(R.id.graph)
    GraphView graph;
    @BindView(R.id.loading)
    ProgressBar loading;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        ButterKnife.bind(this);

        setupGraphView();
    }

    protected void setupGraphView() {
        graph.getViewport().setScrollable(true);
        graph.getViewport().setScalable(true);
        graph.getViewport().setMinX(0);
        graph.getViewport().setMaxX(getMaxItemsInViewPort());
        graph.getGridLabelRenderer().setVerticalAxisTitle(getString(R.string.value));
        graph.getGridLabelRenderer().setHorizontalAxisTitle(getString(R.string.entry));

        String graphTitle = getGraphTitle();
        graph.setTitle(graphTitle);
    }

    protected int getMaxItemsInViewPort() {
        return MAX_ITEMS_IN_VIEWPORT;
    }

    protected void showGraph(final List<? extends StateValueData> data) {
        showBarGraph(data);
        showLineGraph(data);
        showPointGraph(data);
    }

    private void showBarGraph(final List<? extends StateValueData> data) {
        if (SettingsPreferencesUtil.isBarGraphEnabled()) {
            BarGraphSeries<DataPoint> series = createDataPointBarGraphSeries(data);

            configureBarGraphSeries(series);

            graph.addSeries(series);
            scrollToEndIfNecessary(series);
        }
    }

    protected void configureBarGraphSeries(final BarGraphSeries<DataPoint> series) {
        series.setSpacing(SERIES_ITEMS_SPACING);
        series.setDrawValuesOnTop(true);
        series.setValuesOnTopColor(SettingsPreferencesUtil.getBarGraphValueOnTopColor());
        setSeriesTapListener(series);
        setSeriesItemsColors(series);
    }

    private void setSeriesItemsColors(final BarGraphSeries<DataPoint> series) {
        series.setValueDependentColor(d -> {
            StateValueDataPoint data = (StateValueDataPoint) d;
            switch (data.getData().getState()) {
                case AVG:
                default:
                    return SettingsPreferencesUtil.getBarGraphAVGVColor();
                case BELOW_AVG:
                    return SettingsPreferencesUtil.getBarGraphBelowAVGColor();
                case ABOVE_AVG:
                    return SettingsPreferencesUtil.getBarGraphAboveAVGColor();
                case ABNORMALITY:
                    return Color.RED;
            }
        });
    }

    private void showLineGraph(final List<? extends StateValueData> data) {
        if (SettingsPreferencesUtil.isLineGraphEnabled()) {
            LineGraphSeries<DataPoint> series = createDataPointLineGraphSeries(data);

            configureLineGraphSeries(series);

            graph.addSeries(series);
            scrollToEndIfNecessary(series);
        }
    }

    protected void configureLineGraphSeries(final LineGraphSeries<DataPoint> series) {
        setSeriesTapListener(series);
        series.setDrawDataPoints(true);
        series.setColor(SettingsPreferencesUtil.getLineGraphColor());
        series.setThickness(10);
        series.setDataPointsRadius(10);
    }

    private void showPointGraph(final List<? extends StateValueData> data) {
        if (SettingsPreferencesUtil.isPointGraphEnabled()) {
            PointsGraphSeries<DataPoint> series = createDataPointPointsGraphSeries(data);

            configurePointsGraphSeries(series);

            graph.addSeries(series);
            scrollToEndIfNecessary(series);
        }
    }

    protected void configurePointsGraphSeries(final PointsGraphSeries<DataPoint> series) {
        setSeriesTapListener(series);
        series.setShape(SettingsPreferencesUtil.getPointsGraphShape());
        series.setColor(SettingsPreferencesUtil.getPointsGraphColor());
        series.setSize(20);
    }

    private void setSeriesTapListener(final BaseSeries<DataPoint> series) {
        series.setOnDataPointTapListener((series1, dataPoint) -> {
            Date date = new Date(((StateValueDataPoint) dataPoint).getData().getTimestamp());
            Toast.makeText(this, date.toString(), Toast.LENGTH_LONG).show();
        });
    }

    private void scrollToEndIfNecessary(final BaseSeries<DataPoint> series) {
        if (series.getHighestValueX() > getMaxItemsInViewPort()) {
            graph.getViewport().scrollToEnd();
        }
    }

    @NonNull
    protected BarGraphSeries<DataPoint> createDataPointBarGraphSeries(
            final List<? extends StateValueData> data) {
        BarGraphSeries<DataPoint> series = new BarGraphSeries<>();
        addDataToSeries(data, series);
        return series;
    }

    @NonNull
    protected LineGraphSeries<DataPoint> createDataPointLineGraphSeries(
            final List<? extends StateValueData> data) {
        LineGraphSeries<DataPoint> series = new LineGraphSeries<>();
        addDataToSeries(data, series);
        return series;
    }

    @NonNull
    private PointsGraphSeries<DataPoint> createDataPointPointsGraphSeries(
            final List<? extends StateValueData> data) {
        PointsGraphSeries<DataPoint> series = new PointsGraphSeries<>();
        addDataToSeries(data, series);
        return series;
    }

    private void addDataToSeries(final List<? extends StateValueData> data,
                                 final BaseSeries<DataPoint> series) {
        int x = 0;
        for (StateValueData stateValueData : data) {
            int y = (int) stateValueData.getValue();
            series.appendData(new StateValueDataPoint(x++, y, stateValueData), true, 100);
        }
    }

    protected void showLoadingState() {
        loading.setVisibility(View.VISIBLE);
    }

    protected void hideLoadingState() {
        loading.setVisibility(View.GONE);
    }

    protected void onError(final Throwable throwable) {
        hideLoadingState();
        throwable.printStackTrace();
        Toast.makeText(this, R.string.something_went_wrong, Toast.LENGTH_LONG).show();
    }

    protected abstract String getGraphTitle();
}
