package com.wegroszta.andrei.licenta.ui.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.preference.PreferenceManager;

import com.jjoe64.graphview.series.PointsGraphSeries;

public class SettingsPreferencesUtil {
    @SuppressLint("StaticFieldLeak")
    private static Context context;

    private SettingsPreferencesUtil() {
    }

    public static void setContext(final Context context) {
        SettingsPreferencesUtil.context = context;
    }

    private static SharedPreferences getDefaultSharedPref() {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static boolean isBarGraphEnabled() {
        SharedPreferences sharedPreferences = getDefaultSharedPref();
        return sharedPreferences.getBoolean(Keys.BAR_GRAPH_SERIES_KEY.value, true);
    }

    public static boolean isLineGraphEnabled() {
        SharedPreferences sharedPreferences = getDefaultSharedPref();
        return sharedPreferences.getBoolean(Keys.LINE_GRAPH_SERIES_KEY.value, false);
    }

    public static boolean isPointGraphEnabled() {
        SharedPreferences sharedPreferences = getDefaultSharedPref();
        return sharedPreferences.getBoolean(Keys.POINT_GRAPH_SERIES_KEY.value, false);
    }

    public static int getBarGraphValueOnTopColor() {
        SharedPreferences sharedPreferences = getDefaultSharedPref();
        String colorCode = sharedPreferences.getString(Keys.BAR_GRAPH_TOP_VALUE_COLOR.value, "#ff000000");
        return Color.parseColor(colorCode);
    }

    public static int getBarGraphAVGVColor() {
        SharedPreferences sharedPreferences = getDefaultSharedPref();
        String colorCode = sharedPreferences.getString(Keys.BAR_GRAPH_AVG_COLOR.value, "#ff0099cc");
        return Color.parseColor(colorCode);
    }

    public static int getBarGraphBelowAVGColor() {
        SharedPreferences sharedPreferences = getDefaultSharedPref();
        String colorCode = sharedPreferences.getString(Keys.BAR_GRAPH_BELOW_AVG_COLOR.value, "#ff0099cc");
        return Color.parseColor(colorCode);
    }

    public static int getBarGraphAboveAVGColor() {
        SharedPreferences sharedPreferences = getDefaultSharedPref();
        String colorCode = sharedPreferences.getString(Keys.BAR_GRAPH_ABOVE_AVG_COLOR.value, "#ff0099cc");
        return Color.parseColor(colorCode);
    }

    public static int getLineGraphColor() {
        SharedPreferences sharedPreferences = getDefaultSharedPref();
        String colorCode = sharedPreferences.getString(Keys.LINE_GRAPH_COLOR_KEY.value, "#ff0099cc");
        return Color.parseColor(colorCode);
    }

    public static int getPointsGraphColor() {
        SharedPreferences sharedPreferences = getDefaultSharedPref();
        String colorCode = sharedPreferences.getString(Keys.POINTS_GRAPH_COLOR_KEY.value, "#ff0099cc");
        return Color.parseColor(colorCode);
    }

    public static PointsGraphSeries.Shape getPointsGraphShape() {
        SharedPreferences sharedPreferences = getDefaultSharedPref();
        String shape = sharedPreferences.getString(Keys.POINTS_GRAPH_SHAPE_KEY.value, "point");
        switch (shape) {
            case "point":
            default:
                return PointsGraphSeries.Shape.POINT;
            case "triangle":
                return PointsGraphSeries.Shape.TRIANGLE;
            case "rectangle":
                return PointsGraphSeries.Shape.RECTANGLE;
        }
    }

    private enum Keys {
        BAR_GRAPH_SERIES_KEY("bar_graph_series_key"),
        LINE_GRAPH_SERIES_KEY("line_graph_series_key"),
        POINT_GRAPH_SERIES_KEY("point_graph_series_key"),

        //bar graph
        BAR_GRAPH_TOP_VALUE_COLOR("bar_graph_top_value_color_key"),
        BAR_GRAPH_AVG_COLOR("bar_graph_avg_color_key"),
        BAR_GRAPH_BELOW_AVG_COLOR("bar_graph_blow_avg_color_key"),
        BAR_GRAPH_ABOVE_AVG_COLOR("bar_graph_above_avg_color_key"),

        //line graph
        LINE_GRAPH_COLOR_KEY("line_graph_color_key"),

        //points graph
        POINTS_GRAPH_SHAPE_KEY("points_graph_shape_key"),
        POINTS_GRAPH_COLOR_KEY("points_graph_color_key");

        private final String value;

        Keys(String key) {
            value = key;
        }
    }
}
