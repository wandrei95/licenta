package com.wegroszta.andrei.licenta.usecases.statistics.heartrate;

import com.wegroszta.andrei.licenta.entities.HeartRate;

import java.util.List;

import io.reactivex.Observable;

public class FetchHeartRateStatistics {
    private final HeartRateStatisticsFetcher heartRateStatisticsFetcher;

    public FetchHeartRateStatistics(final HeartRateStatisticsFetcher heartRateStatisticsFetcher) {
        this.heartRateStatisticsFetcher = heartRateStatisticsFetcher;
    }

    public Observable<List<HeartRate>> observeHeartRateStatistics(final String userId) {
        return heartRateStatisticsFetcher.getHeartRate(userId);
    }
}
