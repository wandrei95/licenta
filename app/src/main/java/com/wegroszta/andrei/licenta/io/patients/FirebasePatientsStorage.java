package com.wegroszta.andrei.licenta.io.patients;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.wegroszta.andrei.licenta.entities.Patient;
import com.wegroszta.andrei.licenta.usecases.patients.PatientsStorage;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;

public class FirebasePatientsStorage implements PatientsStorage {
    private final DatabaseReference baseDatabaseReference;

    public FirebasePatientsStorage(final DatabaseReference baseDatabaseReference) {
        this.baseDatabaseReference = baseDatabaseReference;
    }

    @Override
    public Observable<Patient> getPatient(final String id) {
        return Observable.create(source -> fetchPatient(id, source));
    }

    @Override
    public Observable<Void> savePatient(final Patient patient) {
        return Observable.create(source -> savePatient(patient, source));
    }

    private void savePatient(final Patient patient, final ObservableEmitter<Void> source) {
        getDatabaseReferenceForPatientDetails(patient.getId())
                .setValue(patient)
                .addOnCompleteListener(task -> source.onComplete())
                .addOnFailureListener(source::onError);
    }

    private void fetchPatient(final String id, final ObservableEmitter<Patient> source) {
        DatabaseReference databaseReference = getDatabaseReferenceForPatientDetails(id);
        databaseReference.addListenerForSingleValueEvent(createValueEventListener(source));
    }

    private ValueEventListener createValueEventListener(final ObservableEmitter<Patient> source) {
        return new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                readPatient(dataSnapshot, source);
            }

            @Override
            public void onCancelled(final DatabaseError databaseError) {
                source.onError(databaseError.toException());
            }
        };
    }

    private void readPatient(final DataSnapshot dataSnapshot,
                             final ObservableEmitter<Patient> source) {
        Patient patient = dataSnapshot.getValue(Patient.class);
        source.onNext(patient);
        source.onComplete();
    }

    private DatabaseReference getDatabaseReferenceForPatientDetails(final String id) {
        return baseDatabaseReference
                .child("patients")
                .child(id)
                .child("details");
    }
}
