package com.wegroszta.andrei.licenta.ui.common;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.wegroszta.andrei.licenta.R;

public class BottomNavigationView extends LinearLayout implements View.OnClickListener {

    private ImageView ivMeasurements;
    private ImageView ivStatistics;
    private ImageView ivDoctors;
    private ImageView ivMyProfile;
    private ImageView ivSettings;

    private OnNavigationClickListener onNavigationClickListener;

    public BottomNavigationView(final Context context) {
        this(context, null);
    }

    public BottomNavigationView(final Context context, @Nullable final AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public BottomNavigationView(final Context context, @Nullable final AttributeSet attrs,
                                final int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        LayoutInflater.from(context).inflate(R.layout.layout_bottom_navigation_view, this, true);

        ivMeasurements = findViewById(R.id.iv_measurements);
        ivStatistics = findViewById(R.id.iv_statistics);
        ivDoctors = findViewById(R.id.iv_doctors);
        ivMyProfile = findViewById(R.id.iv_my_profile);
        ivSettings = findViewById(R.id.iv_settings);

        findViewById(R.id.ll_nav_item_measurements).setOnClickListener(this);
        findViewById(R.id.ll_nav_item_statistics).setOnClickListener(this);
        findViewById(R.id.ll_nav_item_doctors).setOnClickListener(this);
        findViewById(R.id.ll_nav_item_my_profile).setOnClickListener(this);
        findViewById(R.id.ll_nav_item_settings).setOnClickListener(this);
    }

    @Override
    public void onClick(final View view) {
        if (onNavigationClickListener != null) {
            switch (view.getId()) {
                case R.id.ll_nav_item_measurements:
                    selectMeasurements();
                    onNavigationClickListener.onMeasurementsClicked();
                    break;
                case R.id.ll_nav_item_statistics:
                    selectStatistics();
                    onNavigationClickListener.onStatisticsClicked();
                    break;
                case R.id.ll_nav_item_doctors:
                    selectDoctors();
                    onNavigationClickListener.onDoctorsClicked();
                    break;
                case R.id.ll_nav_item_my_profile:
                    selectMyProfile();
                    onNavigationClickListener.onMyProfileClicked();
                    break;
                case R.id.ll_nav_item_settings:
                    selectSettings();
                    onNavigationClickListener.onSettingsClicked();
                    break;
            }
        }
    }

    private void selectMeasurements() {
        ivMeasurements.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_measurements));
        ivStatistics.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivDoctors.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivMyProfile.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivSettings.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
    }

    private void selectStatistics() {
        ivMeasurements.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivStatistics.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_statistics));
        ivDoctors.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivMyProfile.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivSettings.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
    }

    private void selectDoctors() {
        ivMeasurements.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivStatistics.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivDoctors.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_doctors));
        ivMyProfile.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivSettings.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
    }

    private void selectMyProfile() {
        ivMeasurements.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivStatistics.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivDoctors.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivMyProfile.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_my_profile));
        ivSettings.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
    }

    private void selectSettings() {
        ivMeasurements.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivStatistics.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivDoctors.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivMyProfile.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivSettings.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_settings));
    }

    public void selectPage(final Page page) {
        switch (page) {
            case MEASUREMENTS:
                selectMeasurements();
                break;
            case STATISTICS:
                selectStatistics();
                break;
            case DOCTORS:
                selectDoctors();
                break;
            case MY_PROFILE:
                selectMyProfile();
                break;
            case SETTINGS:
                selectSettings();
                break;
        }
    }

    public void setOnNavigationClickListener(final OnNavigationClickListener onNavigationClickListener) {
        this.onNavigationClickListener = onNavigationClickListener;
    }

    public interface OnNavigationClickListener {
        void onMeasurementsClicked();

        void onStatisticsClicked();

        void onDoctorsClicked();

        void onMyProfileClicked();

        void onSettingsClicked();
    }

    public enum Page {
        MEASUREMENTS,
        STATISTICS,
        DOCTORS,
        MY_PROFILE,
        SETTINGS
    }
}