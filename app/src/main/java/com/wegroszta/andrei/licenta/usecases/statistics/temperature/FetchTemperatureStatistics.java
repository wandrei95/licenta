package com.wegroszta.andrei.licenta.usecases.statistics.temperature;

import com.wegroszta.andrei.licenta.entities.Temperature;

import java.util.List;

import io.reactivex.Observable;

public class FetchTemperatureStatistics {
    private final TemperatureStatisticsFetcher temperatureStatisticsFetcher;

    public FetchTemperatureStatistics(final TemperatureStatisticsFetcher temperatureStatisticsFetcher) {
        this.temperatureStatisticsFetcher = temperatureStatisticsFetcher;
    }

    public Observable<List<Temperature>> observeTemperatureStatistics(final String userId){
        return temperatureStatisticsFetcher.getTemperatures(userId);
    }
}
