package com.wegroszta.andrei.licenta.viewmodels.auth;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.wegroszta.andrei.licenta.entities.Credentials;
import com.wegroszta.andrei.licenta.entities.User;
import com.wegroszta.andrei.licenta.usecases.auth.Login;
import com.wegroszta.andrei.licenta.viewmodels.responses.Response;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class LoginViewModel extends ViewModel {
    private final Login login;
    private final MutableLiveData<Response<Void>> loginResponse;
    private final MutableLiveData<Response<Void>> logoutResponse;
    private final MutableLiveData<Response<Void>> signUpResponse;
    private final CompositeDisposable disposables;

    public LoginViewModel(final Login login) {
        this.login = login;
        loginResponse = new MutableLiveData<>();
        logoutResponse = new MutableLiveData<>();
        signUpResponse = new MutableLiveData<>();
        disposables = new CompositeDisposable();
    }

    @Override
    protected void onCleared() {
        disposables.clear();
    }

    public MutableLiveData<Response<Void>> getLoginResponse() {
        return loginResponse;
    }

    public MutableLiveData<Response<Void>> getLogoutResponse() {
        return logoutResponse;
    }

    public MutableLiveData<Response<Void>> getSignUpResponse() {
        return signUpResponse;
    }

    public void login(final String email, final String password) {
        final Credentials credentials = new Credentials(email, password);
        disposables.add(login.login(credentials)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> loginResponse.setValue(Response.started()))
                .subscribe(
                        aVoid -> loginResponse.setValue(Response.success(null)),
                        throwable -> loginResponse.setValue(Response.failure(throwable)),
                        () -> loginResponse.setValue(Response.completed(null))
                )
        );
    }

    public void logout() {
        disposables.add(login.logout()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> logoutResponse.setValue(Response.started()))
                .subscribe(
                        aVoid -> logoutResponse.setValue(Response.success(null)),
                        throwable -> logoutResponse.setValue(Response.failure(throwable)),
                        () -> logoutResponse.setValue(Response.completed(null))
                )
        );
    }

    public void signUp(final String firstName, final String lastName, final String email, final String password) {
        final Credentials credentials = new Credentials(email, password);
        final User user = new User(firstName, lastName, email);
        disposables.add(login.signUp(credentials, user)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> signUpResponse.setValue(Response.started()))
                .subscribe(
                        aVoid -> signUpResponse.setValue(Response.success(null)),
                        throwable -> signUpResponse.setValue(Response.failure(throwable)),
                        () -> signUpResponse.setValue(Response.completed(null))
                )
        );
    }

    public boolean isUserLoggedIn() {
        return login.isUserLoggedIn();
    }
}
