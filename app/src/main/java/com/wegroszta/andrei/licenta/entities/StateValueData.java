package com.wegroszta.andrei.licenta.entities;

public class StateValueData extends StateData {
    private double value;

    public StateValueData() {
        this(0L, State.AVG, 0d);
    }

    public StateValueData(final long timestamp, final double value) {
        this(timestamp, State.AVG, value);
    }

    public StateValueData(final long timestamp, final State state, final double value) {
        super(timestamp, state);
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}

