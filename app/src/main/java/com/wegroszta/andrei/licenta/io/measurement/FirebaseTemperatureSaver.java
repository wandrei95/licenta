package com.wegroszta.andrei.licenta.io.measurement;

import com.google.firebase.database.DatabaseReference;
import com.wegroszta.andrei.licenta.entities.Temperature;
import com.wegroszta.andrei.licenta.usecases.measurement.temperature.TemperatureSaver;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;

public class FirebaseTemperatureSaver implements TemperatureSaver {
    private final DatabaseReference baseDatabaseReference;

    public FirebaseTemperatureSaver(final DatabaseReference baseDatabaseReference) {
        this.baseDatabaseReference = baseDatabaseReference;
    }

    @Override
    public Observable<Void> saveTemperature(final String userId, final Temperature temperature) {
        return Observable.create(source -> saveTemperature(userId, temperature, source));
    }

    private void saveTemperature(final String userId, final Temperature temperature,
                                 final ObservableEmitter<Void> source) {
        getTemperatureDatabaseReferenceForUser(userId)
                .setValue(temperature)
                .addOnCompleteListener(task -> source.onComplete())
                .addOnFailureListener(source::onError);
    }

    private DatabaseReference getTemperatureDatabaseReferenceForUser(final String userId) {
        return baseDatabaseReference.child("measurement")
                .child("temperature")
                .child(userId)
                .child("values").push();
    }
}
