package com.wegroszta.andrei.licenta.usecases.doctors;

import com.wegroszta.andrei.licenta.entities.Doctor;

import java.util.List;

import io.reactivex.Observable;

public class GetDoctors {
    private final DoctorsStorage doctorsStorage;

    public GetDoctors(DoctorsStorage doctorsStorage) {
        this.doctorsStorage = doctorsStorage;
    }

    public Observable<List<Doctor>> getAllDoctors() {
        return doctorsStorage.getAllDoctors();
    }

    public Observable<List<Doctor>> getDoctorsForPatient(final String patientId) {
        return doctorsStorage.getDoctorsForPatient(patientId);
    }

    public Observable<Doctor> getDoctor(final String doctorId) {
        return doctorsStorage.getDoctor(doctorId);
    }
}
