package com.wegroszta.andrei.licenta.viewmodels.statistics;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.wegroszta.andrei.licenta.entities.Temperature;
import com.wegroszta.andrei.licenta.usecases.auth.UserDataFetcher;
import com.wegroszta.andrei.licenta.usecases.statistics.temperature.FetchTemperatureStatistics;
import com.wegroszta.andrei.licenta.viewmodels.responses.Response;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class FetchTemperatureStatisticsViewModel extends ViewModel {
    private final FetchTemperatureStatistics fetchTemperatureStatistics;
    private final UserDataFetcher userDataFetcher;
    private final MutableLiveData<Response<List<Temperature>>> fetchTemperatureResponse;
    private final CompositeDisposable disposables;


    public FetchTemperatureStatisticsViewModel(final FetchTemperatureStatistics fetchTemperatureStatistics,
                                               final UserDataFetcher userDataFetcher) {
        this.fetchTemperatureStatistics = fetchTemperatureStatistics;
        this.userDataFetcher = userDataFetcher;
        fetchTemperatureResponse = new MutableLiveData<>();
        disposables = new CompositeDisposable();
    }

    @Override
    protected void onCleared() {
        disposables.clear();
    }

    public MutableLiveData<Response<List<Temperature>>> getFetchTemperatureResponse() {
        return fetchTemperatureResponse;
    }

    public void getTemperatures() {
        final String userId = userDataFetcher.getLoggedUserId();
        disposables.add(fetchTemperatureStatistics.observeTemperatureStatistics(userId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> fetchTemperatureResponse.setValue(Response.started()))
                .subscribe(
                        temperatures -> fetchTemperatureResponse.setValue(Response.success(temperatures)),
                        throwable -> fetchTemperatureResponse.setValue(Response.failure(throwable)),
                        () -> fetchTemperatureResponse.setValue(Response.completed(null))
                )
        );
    }
}
