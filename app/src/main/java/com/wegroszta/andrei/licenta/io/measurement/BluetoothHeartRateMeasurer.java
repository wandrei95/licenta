package com.wegroszta.andrei.licenta.io.measurement;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import com.wegroszta.andrei.licenta.entities.HeartRate;
import com.wegroszta.andrei.licenta.entities.State;
import com.wegroszta.andrei.licenta.usecases.measurement.heartrate.HeartRateMeasurer;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;

public class BluetoothHeartRateMeasurer implements HeartRateMeasurer {
    private static final String HEART_RATE_REQUEST_KEY = "heartRate";

    private boolean shouldReadHeartRate;
    private BluetoothSocket bluetoothSocket;

    @Override
    public Observable<HeartRate> observeHeartRate() {
        return Observable.create(source -> {
                    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                    BluetoothDevice bluetoothDevice = getBluetoothDevice(mBluetoothAdapter);
                    if (bluetoothDevice == null) {
                        source.onError(new Exception("Could not find any bounded device"));
                        return;
                    }

                    bluetoothSocket = createBluetoothDevice(bluetoothDevice);

                    try {
                        connectBluetoothSocket(bluetoothSocket);
                        sendHeartRateRequest(bluetoothSocket);
                    } catch (IOException e) {
                        source.onError(new Exception("Could not send heart rate request"));
                        return;
                    }

                    try {
                        shouldReadHeartRate = true;
                        readHeartRate(source, bluetoothSocket);
                        bluetoothSocket.close();
                    } catch (Exception e) {
                    }
                }
        );
    }

    @Override
    public void stopMeasuring() {
        shouldReadHeartRate = false;
        if (bluetoothSocket != null) {
            try {
                bluetoothSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private BluetoothSocket createBluetoothDevice(
            final BluetoothDevice bluetoothDevice) throws IOException {
        UUID uuid = UUID.fromString("94f39d29-7d6d-437d-973b-fba39e49d4ee"); //Standard SerialPortService ID
        return bluetoothDevice.createRfcommSocketToServiceRecord(uuid);
    }

    private void readHeartRate(final ObservableEmitter<HeartRate> source,
                               final BluetoothSocket bluetoothSocket) throws IOException {
        final InputStream inputStream = bluetoothSocket.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        while (shouldReadHeartRate) {
            String msg = reader.readLine();
            Log.e("HeartRateMeasurer", msg);
            try {
                JSONObject jsonObject = new JSONObject(msg);
                long timestamp = jsonObject.getLong("timestamp");
                int value = jsonObject.getInt("value");
                source.onNext(new HeartRate(timestamp, value));
            } catch (Exception e) {
                source.onNext(new HeartRate(System.currentTimeMillis(), State.BELOW_AVG, 0));
            }
        }
        reader.close();
        bluetoothSocket.close();
    }

    private void sendHeartRateRequest(final BluetoothSocket bluetoothSocket) throws IOException {
        OutputStream outputStream = bluetoothSocket.getOutputStream();
        outputStream.write(HEART_RATE_REQUEST_KEY.getBytes());
    }

    private BluetoothDevice getBluetoothDevice(final BluetoothAdapter mBluetoothAdapter) {
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                if (device.getName().equals("raspberrypi")) {
                    return device;
                }
            }
        }
        return null;
    }

    private void connectBluetoothSocket(final BluetoothSocket bluetoothSocket) throws IOException {
        if (!bluetoothSocket.isConnected()) {
            bluetoothSocket.connect();
        }
    }
}