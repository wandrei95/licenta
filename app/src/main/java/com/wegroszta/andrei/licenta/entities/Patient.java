package com.wegroszta.andrei.licenta.entities;

public class Patient extends User {
    private String id;
    private final String phone;
    private final String address;

    public Patient() {
        super();
        id = null;
        phone = null;
        address = null;
    }

    public Patient(final String id, final String firstName, final String lastName,
                   final String email, final String phone, final String address) {
        super(firstName, lastName, email);
        this.id = id;
        this.phone = phone;
        this.address = address;
    }

    public Patient(final String firstName, final String lastName, final String email,
                   final String phone, final String address) {
        super(firstName, lastName, email);
        id = null;
        this.phone = phone;
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public void setId(String id) {
        this.id = id;
    }
}
