package com.wegroszta.andrei.licenta.ui.common;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.wegroszta.andrei.licenta.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity
        implements BottomNavigationView.OnNavigationClickListener, ViewPager.OnPageChangeListener {

    @BindView(R.id.vp_main_screens)
    ViewPager vpMainScreens;
    @BindView(R.id.bottom_navigation_view)
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        bottomNavigationView.setOnNavigationClickListener(this);

        MainScreensFragmentPagerAdapter pagerAdapter =
                new MainScreensFragmentPagerAdapter(getSupportFragmentManager());
        vpMainScreens.setAdapter(pagerAdapter);

        vpMainScreens.addOnPageChangeListener(this);
    }

    @Override
    public void onMeasurementsClicked() {
        vpMainScreens.setCurrentItem(0);
    }

    @Override
    public void onStatisticsClicked() {
        vpMainScreens.setCurrentItem(1);
    }

    @Override
    public void onDoctorsClicked() {
        vpMainScreens.setCurrentItem(2);
    }

    @Override
    public void onMyProfileClicked() {
        vpMainScreens.setCurrentItem(3);
    }

    @Override
    public void onSettingsClicked() {
        vpMainScreens.setCurrentItem(4);
    }

    @Override
    public void onPageScrolled(final int position, final float positionOffset,
                               final int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(final int position) {
        switch (position) {
            case 0:
                bottomNavigationView.selectPage(BottomNavigationView.Page.MEASUREMENTS);
                break;
            case 1:
                bottomNavigationView.selectPage(BottomNavigationView.Page.STATISTICS);
                break;
            case 2:
                bottomNavigationView.selectPage(BottomNavigationView.Page.DOCTORS);
                break;
            case 3:
                bottomNavigationView.selectPage(BottomNavigationView.Page.MY_PROFILE);
                break;
            case 4:
                bottomNavigationView.selectPage(BottomNavigationView.Page.SETTINGS);
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(final int state) {

    }
}
