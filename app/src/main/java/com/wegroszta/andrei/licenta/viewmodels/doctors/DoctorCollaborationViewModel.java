package com.wegroszta.andrei.licenta.viewmodels.doctors;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.wegroszta.andrei.licenta.usecases.auth.UserDataFetcher;
import com.wegroszta.andrei.licenta.usecases.doctors.RequestDoctorCollaboration;
import com.wegroszta.andrei.licenta.usecases.patients.PatientsInteractor;
import com.wegroszta.andrei.licenta.viewmodels.responses.Response;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class DoctorCollaborationViewModel extends ViewModel {
    private final RequestDoctorCollaboration requestDoctorCollaboration;
    private final UserDataFetcher userDataFetcher;
    private final PatientsInteractor patientsInteractor;
    private final MutableLiveData<Response<Void>> requestDoctorCollaborationResponse;
    private final MutableLiveData<Response<Void>> finishDoctorCollaborationResponse;
    private final MutableLiveData<Response<Boolean>> checkDoctorAssociationResponse;
    private final CompositeDisposable disposables;

    public DoctorCollaborationViewModel(final RequestDoctorCollaboration requestDoctorCollaboration,
                                        final UserDataFetcher userDataFetcher,
                                        final PatientsInteractor patientsInteractor) {
        this.requestDoctorCollaboration = requestDoctorCollaboration;
        this.userDataFetcher = userDataFetcher;
        this.patientsInteractor = patientsInteractor;
        requestDoctorCollaborationResponse = new MutableLiveData<>();
        finishDoctorCollaborationResponse = new MutableLiveData<>();
        checkDoctorAssociationResponse = new MutableLiveData<>();
        disposables = new CompositeDisposable();
    }

    @Override
    protected void onCleared() {
        disposables.clear();
    }

    public MutableLiveData<Response<Void>> getRequestDoctorCollaborationResponse() {
        return requestDoctorCollaborationResponse;
    }

    public MutableLiveData<Response<Void>> getFinishDoctorCollaborationResponse() {
        return finishDoctorCollaborationResponse;
    }

    public MutableLiveData<Response<Boolean>> getCheckDoctorAssociationResponse() {
        return checkDoctorAssociationResponse;
    }

    public void requestDoctorCollaboration(final String doctorId) {
        final String userId = userDataFetcher.getLoggedUserId();
        patientsInteractor.getPatient(userId)
                .subscribeOn(Schedulers.newThread())
                .subscribe(patient -> disposables.add(requestDoctorCollaboration.requestCollaboration(
                        userId, patient.getFirstName(), patient.getLastName(), doctorId)
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                                .doOnSubscribe(disposable -> requestDoctorCollaborationResponse.setValue(Response.started()))
                                .subscribe(
                                        aVoid -> requestDoctorCollaborationResponse.setValue(Response.success(aVoid)),
                                        throwable -> requestDoctorCollaborationResponse.setValue(Response.failure(throwable)),
                                        () -> requestDoctorCollaborationResponse.setValue(Response.completed(null))
                                )),
                        throwable -> requestDoctorCollaborationResponse.setValue(Response.failure(throwable)));
    }

    public void finishDoctorCollaboration(final String doctorId) {
        final String userId = userDataFetcher.getLoggedUserId();
        patientsInteractor.getPatient(userId)
                .subscribeOn(Schedulers.newThread())
                .subscribe(patient -> disposables.add(requestDoctorCollaboration.finishCollaboration(userId, doctorId)
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                                .doOnSubscribe(disposable -> finishDoctorCollaborationResponse.setValue(Response.started()))
                                .subscribe(
                                        aVoid -> finishDoctorCollaborationResponse.setValue(Response.success(aVoid)),
                                        throwable -> finishDoctorCollaborationResponse.setValue(Response.failure(throwable)),
                                        () -> finishDoctorCollaborationResponse.setValue(Response.completed(null))
                                )),
                        throwable -> finishDoctorCollaborationResponse.setValue(Response.failure(throwable)));
    }

    public void checkDoctorAssociationWithLoggedUser(String doctorId) {
        final String userId = userDataFetcher.getLoggedUserId();
        disposables.add(requestDoctorCollaboration.checkDoctorAssociation(userId, doctorId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> checkDoctorAssociationResponse.setValue(Response.started()))
                .subscribe(
                        idDoctorAssociatedWithUser -> checkDoctorAssociationResponse.setValue(Response.success(idDoctorAssociatedWithUser)),
                        throwable -> checkDoctorAssociationResponse.setValue(Response.failure(throwable)),
                        () -> checkDoctorAssociationResponse.setValue(Response.completed(null))
                )
        );
    }
}
