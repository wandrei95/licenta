package com.wegroszta.andrei.licenta.entities;

public class User {
    private final String firstName;
    private final String lastName;
    private final String email;

    public User() {
        this(null, null, null);
    }

    public User(final String firstName, final String lastName, final String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }
}
