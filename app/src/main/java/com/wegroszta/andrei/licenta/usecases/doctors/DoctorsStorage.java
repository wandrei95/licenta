package com.wegroszta.andrei.licenta.usecases.doctors;

import com.wegroszta.andrei.licenta.entities.Doctor;

import java.util.List;

import io.reactivex.Observable;

public interface DoctorsStorage {
    Observable<List<Doctor>> getAllDoctors();

    Observable<List<Doctor>> getDoctorsForPatient(String patientId);

    Observable<Doctor> getDoctor(String doctorId);
}
