package com.wegroszta.andrei.licenta.io.doctors;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.wegroszta.andrei.licenta.entities.Doctor;
import com.wegroszta.andrei.licenta.usecases.doctors.DoctorsStorage;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;

public class FirebaseDoctorsStorage implements DoctorsStorage {
    private final DatabaseReference baseDatabaseReference;

    public FirebaseDoctorsStorage(final DatabaseReference baseDatabaseReference) {
        this.baseDatabaseReference = baseDatabaseReference;
    }

    @Override
    public Observable<List<Doctor>> getAllDoctors() {
        return Observable.create(this::fetchAllDoctors);
    }

    @Override
    public Observable<List<Doctor>> getDoctorsForPatient(final String patientId) {
        return Observable.create(source -> fetchDoctorsForPatients(source, patientId));
    }

    @Override
    public Observable<Doctor> getDoctor(String doctorId) {
        return Observable.create(source -> getDoctor(source, doctorId));
    }

    private void fetchAllDoctors(final ObservableEmitter<List<Doctor>> source) {
        DatabaseReference databaseReference = getDatabaseReference();
        databaseReference.addListenerForSingleValueEvent(createValueEventListener(source));
    }

    private ValueEventListener createValueEventListener(
            final ObservableEmitter<List<Doctor>> source) {
        return new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                readDoctors(dataSnapshot, source);
            }

            @Override
            public void onCancelled(final DatabaseError databaseError) {
                source.onError(databaseError.toException());
            }
        };
    }

    private void readDoctors(final DataSnapshot dataSnapshot,
                             final ObservableEmitter<List<Doctor>> source) {
        List<Doctor> doctors = getDoctorsFromDataSnapshot(dataSnapshot);
        source.onNext(doctors);
        source.onComplete();
    }

    @NonNull
    private List<Doctor> getDoctorsFromDataSnapshot(final DataSnapshot dataSnapshot) {
        List<Doctor> doctors = new ArrayList<>();
        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
            Doctor doctor = postSnapshot.child("details").getValue(Doctor.class);
            if (doctor != null) {
                doctor.setId(postSnapshot.getKey());
                doctors.add(doctor);
            }
        }
        return doctors;
    }

    private DatabaseReference getDatabaseReference() {
        return baseDatabaseReference
                .child("doctors");
    }

    private void fetchDoctorsForPatients(final ObservableEmitter<List<Doctor>> source,
                                         final String patientId) {
        DatabaseReference databaseReference = getDatabaseReferenceForPatientsDoctors(patientId);
        databaseReference.addListenerForSingleValueEvent(createValueEventListenerForPatientsDoctors(source));
    }

    private ValueEventListener createValueEventListenerForPatientsDoctors(
            final ObservableEmitter<List<Doctor>> source) {
        return new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                readPatientsDoctors(dataSnapshot, source);
            }

            @Override
            public void onCancelled(final DatabaseError databaseError) {
                source.onError(databaseError.toException());
            }
        };
    }

    private void readPatientsDoctors(final DataSnapshot dataSnapshot,
                                     final ObservableEmitter<List<Doctor>> source) {
        List<Doctor> doctors = getPatientsDoctorsFromDataSnapshot(dataSnapshot);
        source.onNext(doctors);
        source.onComplete();
    }

    @NonNull
    private List<Doctor> getPatientsDoctorsFromDataSnapshot(final DataSnapshot dataSnapshot) {
        List<Doctor> doctors = new ArrayList<>();
        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
            Doctor doctor = postSnapshot.getValue(Doctor.class);
            if (doctor != null) {
                doctor.setId(postSnapshot.getKey());
                doctors.add(doctor);
            }
        }
        return doctors;
    }

    private DatabaseReference getDatabaseReferenceForPatientsDoctors(final String patientId) {
        return baseDatabaseReference
                .child("patients")
                .child(patientId)
                .child("myDoctors");
    }

    private void getDoctor(final ObservableEmitter<Doctor> source, final String doctorId) {
        DatabaseReference databaseReference = getDatabaseReferenceForDoctorDetails(doctorId);
        databaseReference.addListenerForSingleValueEvent(createValueEventListenerForDoctorDetails(source));
    }

    private ValueEventListener createValueEventListenerForDoctorDetails(
            final ObservableEmitter<Doctor> source) {
        return new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                readDoctor(dataSnapshot, source);
            }

            @Override
            public void onCancelled(final DatabaseError databaseError) {
                source.onError(databaseError.toException());
            }
        };
    }

    private void readDoctor(final DataSnapshot dataSnapshot,
                            final ObservableEmitter<Doctor> source) {
        Doctor doctor = dataSnapshot.getValue(Doctor.class);
        source.onNext(doctor);
        source.onComplete();
    }

    private DatabaseReference getDatabaseReferenceForDoctorDetails(final String id) {
        return FirebaseDatabase.getInstance().getReference()
                .child("doctors")
                .child(id)
                .child("details");
    }
}