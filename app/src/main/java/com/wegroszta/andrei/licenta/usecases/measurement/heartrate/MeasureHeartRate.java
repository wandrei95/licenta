package com.wegroszta.andrei.licenta.usecases.measurement.heartrate;

import com.wegroszta.andrei.licenta.entities.HeartRate;
import com.wegroszta.andrei.licenta.entities.State;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.functions.Function;

public class MeasureHeartRate {
    private static final int HEART_RATE_MIN_AVG_VALUE = 60;
    private static final int HEART_RATE_MAX_AVG_VALUE = 100;
    private final HeartRateSaver heartRateSaver;
    private final HeartRateMeasurer heartRateMeasurer;

    private double measuredHeartRateAvg = 0;

    public MeasureHeartRate(final HeartRateSaver heartRateSaver,
                            final HeartRateMeasurer heartRateMeasurer) {
        this.heartRateSaver = heartRateSaver;
        this.heartRateMeasurer = heartRateMeasurer;
    }

    public Observable<HeartRate> observeHeartRate() {
        measuredHeartRateAvg = 0d;
        return heartRateMeasurer.observeHeartRate().map((Function<HeartRate, HeartRate>) heartRate -> {
            computeMeasuredHeartRateAvg(heartRate);
            State state = computeHeartRateState(heartRate.getValue());
            return new HeartRate(heartRate.getTimestamp(), state, (int) measuredHeartRateAvg);
        });
//        return Observable.create(source ->
//                heartRateMeasurer.observeHeartRate()
//                        .subscribe(heartRate -> handleMeasuredHeartRate(source, heartRate),
//                                throwable -> handleMeasuredHeartRateError(source, throwable)));
    }

    private void handleMeasuredHeartRateError(ObservableEmitter<HeartRate> source, Throwable throwable) {
        if (!source.isDisposed()) {
            source.onError(throwable);
        }
    }

    public void stopMeasuring() {
        heartRateMeasurer.stopMeasuring();
    }

    private void handleMeasuredHeartRate(final ObservableEmitter<HeartRate> source,
                                         final HeartRate heartRate) {
        computeMeasuredHeartRateAvg(heartRate);
        State state = computeHeartRateState(heartRate.getValue());
        source.onNext(new HeartRate(heartRate.getTimestamp(), state, (int) measuredHeartRateAvg));
    }

    private void computeMeasuredHeartRateAvg(final HeartRate heartRate) {
        if (measuredHeartRateAvg == 0d) {
            measuredHeartRateAvg = heartRate.getValue();
        } else {
            measuredHeartRateAvg = (measuredHeartRateAvg + heartRate.getValue()) / 2;
        }
    }

    private State computeHeartRateState(final double heartRateValue) {
        if (heartRateValue <= HEART_RATE_MIN_AVG_VALUE) {
            return State.BELOW_AVG;
        } else if (heartRateValue <= HEART_RATE_MAX_AVG_VALUE) {
            return State.AVG;
        } else {
            return State.ABOVE_AVG;
        }
    }

    public Observable<Void> saveHeartRate(final String userId, final int value) {
        HeartRate heartRate = new HeartRate(System.currentTimeMillis(), computeHeartRateState(value), value);
        return heartRateSaver.saveHeartRate(userId, heartRate);
    }
}
