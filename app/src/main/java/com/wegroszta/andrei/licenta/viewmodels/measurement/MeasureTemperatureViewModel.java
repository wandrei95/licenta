package com.wegroszta.andrei.licenta.viewmodels.measurement;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.wegroszta.andrei.licenta.entities.Temperature;
import com.wegroszta.andrei.licenta.usecases.auth.UserDataFetcher;
import com.wegroszta.andrei.licenta.usecases.measurement.temperature.MeasureTemperature;
import com.wegroszta.andrei.licenta.viewmodels.responses.Response;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MeasureTemperatureViewModel extends ViewModel {
    private final MeasureTemperature measureTemperature;
    private final UserDataFetcher userDataFetcher;
    private final MutableLiveData<Response<Temperature>> measureTemperatureResponse;
    private final MutableLiveData<Response<Void>> saveTemperatureResponse;
    private final CompositeDisposable disposables;

    public MeasureTemperatureViewModel(final MeasureTemperature measureTemperature,
                                       final UserDataFetcher userDataFetcher) {
        this.measureTemperature = measureTemperature;
        this.userDataFetcher = userDataFetcher;
        measureTemperatureResponse = new MutableLiveData<>();
        saveTemperatureResponse = new MutableLiveData<>();
        disposables = new CompositeDisposable();
    }

    @Override
    public void onCleared() {
        measureTemperature.stopMeasuring();
        disposables.clear();
    }

    public MutableLiveData<Response<Temperature>> getMeasureTemperatureResponse() {
        return measureTemperatureResponse;
    }

    public MutableLiveData<Response<Void>> getSaveTemperatureResponse() {
        return saveTemperatureResponse;
    }

    public void measureTemperature() {
        disposables.add(measureTemperature.observeTemperature()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> measureTemperatureResponse.setValue(Response.started()))
                .subscribe(
                        temperature -> measureTemperatureResponse.setValue(Response.success(temperature)),
                        throwable -> measureTemperatureResponse.setValue(Response.failure(throwable)),
                        () -> measureTemperatureResponse.setValue(Response.completed(null))
                )
        );
    }

    public void saveTemperature(final double value) {
        final String userId = userDataFetcher.getLoggedUserId();
        disposables.add(measureTemperature.saveTemperature(userId, value)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> saveTemperatureResponse.setValue(Response.started()))
                .subscribe(
                        aVoid -> saveTemperatureResponse.setValue(Response.success(aVoid)),
                        throwable -> saveTemperatureResponse.setValue(Response.failure(throwable)),
                        () -> saveTemperatureResponse.setValue(Response.completed(null))
                )
        );
    }
}
