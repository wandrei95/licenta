package com.wegroszta.andrei.licenta.ui.auth;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.wegroszta.andrei.licenta.R;
import com.wegroszta.andrei.licenta.ui.util.ValidationUtil;
import com.wegroszta.andrei.licenta.ui.common.ViewModelFactory;
import com.wegroszta.andrei.licenta.viewmodels.auth.LoginViewModel;
import com.wegroszta.andrei.licenta.viewmodels.responses.Response;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.et_first_name)
    EditText etFirstName;
    @BindView(R.id.et_last_name)
    EditText etLastName;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.loading)
    ProgressBar loading;

    private LoginViewModel loginViewModel;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        ButterKnife.bind(this);

        setClickListeners();

        setupLoginViewModel();
    }

    private void setClickListeners() {
        findViewById(R.id.btn_sign_up).setOnClickListener(this);
        findViewById(R.id.tv_login).setOnClickListener(this);
    }

    @Override
    public void onClick(final View view) {
        switch (view.getId()) {
            case R.id.btn_sign_up:
                registerUser();
                break;
            case R.id.tv_login:
                goToLogin();
                break;
        }
    }

    private void setupLoginViewModel() {
        loginViewModel = ViewModelProviders.of(this, ViewModelFactory.getInstance())
                .get(LoginViewModel.class);
        loginViewModel.getSignUpResponse().observe(this, this::processLoginResponse);
    }

    private void processLoginResponse(final Response<Void> loginResponse) {
        switch (loginResponse.getState()) {
            case STARTED:
                showLoadingState();
                break;
            case SUCCESS:
                break;
            case COMPLETED:
                signUpSuccess();
                break;
            case FAILURE:
                signUpFail(loginResponse.getError());
                break;
        }
    }

    private void goToLogin() {
        onBackPressed();
    }

    private void hideLoadingState() {
        loading.setVisibility(View.GONE);
    }

    private void showLoadingState() {
        loading.setVisibility(View.VISIBLE);
    }

    private void registerUser() {
        String firstName = etFirstName.getText().toString();
        String lastName = etLastName.getText().toString();
        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        if (validateSignUpData(firstName, lastName, email, password)) {
            loginViewModel.signUp(firstName, lastName, email, password);
        }
    }

    private boolean validateSignUpData(final String firstName, final String lasName,
                                       final String email, final String password) {
        return isFirstNameValid(firstName) && isLastNameValid(lasName)
                && isEmailValid(email) && isPasswordValid(password);
    }

    private boolean isFirstNameValid(final String firstName) {
        if (ValidationUtil.isStringNullOrEmpty(firstName)) {
            etFirstName.setError(getString(R.string.empty_first_name_msg));
            etFirstName.requestFocus();
            return false;
        }
        return true;
    }

    private boolean isLastNameValid(final String lasName) {
        if (ValidationUtil.isStringNullOrEmpty(lasName)) {
            etLastName.setError(getString(R.string.empty_last_name_msg));
            etLastName.requestFocus();
            return false;
        }
        return true;
    }

    private boolean isEmailValid(final String email) {
        return !isEmailEmpty(email) && !isEmailFormatInvalid(email);
    }

    private boolean isEmailEmpty(final String email) {
        if (ValidationUtil.isStringNullOrEmpty(email)) {
            etEmail.setError(getString(R.string.empty_email_msg));
            etEmail.requestFocus();
            return true;
        }
        return false;
    }

    private boolean isPasswordValid(final String password) {
        return !isPasswordEmpty(password) && !isPasswordLengthInvalid(password);
    }

    private boolean isEmailFormatInvalid(final String email) {
        if (ValidationUtil.isEmailFormatInvalid(email)) {
            etEmail.setError(getString(R.string.invalid_email_format_msg));
            etEmail.requestFocus();
            return true;
        }
        return false;
    }

    private boolean isPasswordEmpty(final String password) {
        if (ValidationUtil.isStringNullOrEmpty(password)) {
            etPassword.setError(getString(R.string.empty_password_msg));
            etPassword.requestFocus();
            return true;
        }
        return false;
    }

    private boolean isPasswordLengthInvalid(final String password) {
        if (ValidationUtil.isPasswordLengthInvalid(password)) {
            etPassword.setError(getString(R.string.too_short_password_msg));
            etPassword.requestFocus();
            return true;
        }
        return false;
    }

    private void signUpFail(final Throwable throwable) {
        hideLoadingState();
        Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
    }

    private void signUpSuccess() {
        hideLoadingState();
        finish();
    }
}